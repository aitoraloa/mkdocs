# Applications and Implications 
 
What will it do?

Who's done what beforehand?

What will you design?

What materials and components will be used?

Where will come from?

How much will they cost?

What parts and systems will be made?

What processes will be used?

What questions need to be answered?

How will it be evaluated?

Your project should incorporate 2D and 3D design, additive and subtractive fabrication processes, electronics design and production, microcontroller interfacing and programming, system integration and packaging

Where possible, you should make rather than buy the parts of your project

Projects can be separate or joint, but need to show individual mastery of the skills, and be independently operable 