# Electronics Design

For [this week’s](http://academy.cba.mit.edu/classes/electronics_design/index.html) individual assignment I had to redraw the [echo hello-world (EHW) board](http://academy.cba.mit.edu/classes/embedded_programming/index.html#echo) using any of the [Electronic Design Automation (EDA) software](https://en.wikipedia.org/wiki/Electronic_design_automation) packages mentioned during the weekly lecture, and adding (at least) one [push-button](https://en.wikipedia.org/wiki/Push-button) and one [LED (Light Emitting Diode)](https://en.wikipedia.org/wiki/Light-emitting_diode) to the circuit. 

With the EHW board redrawn, we had to perform a [design rules check](https://en.wikipedia.org/wiki/Design_rule_checking), fabricate it, and test it.

Furthermore, as group project for this week, we had to use the test equipment available in the fab lab ([power supply](https://en.wikipedia.org/wiki/Power_supply), [multimeter](https://en.wikipedia.org/wiki/Multimeter) and [oscilloscope](https://en.wikipedia.org/wiki/Oscilloscope)) in order to observe the operation of the [microcontroller](https://en.wikipedia.org/wiki/Microcontroller) in the circuit board that we had fabricated during the individual project.



## **Getting to know the EHW board**

The echo hello-world board is a simple board with an [ATTiny44 micro-controller](https://www.digikey.es/product-detail/es/microchip-technology/ATTINY44A-SSU/ATTINY44A-SSU-ND/1914708) built-in that a computer can talk to, and that it can talk back to the computer, thanks to its little processor.


![](img/electronics-design/getting-to-know-the-echo-hello-world-board/getting-to-know-the-echo-hello-world-board-01.png)


As I had done two weeks before during the Electronics Production week, I started this assignment by downloading all the PNG files related to the echo hello-world board that I was able to find: [board](http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.44.png), [components](http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.44.components.jpg), [traces](http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.44.traces.png), [interior](http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.44.interior.png), [programming](http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.44.program.png), [mods](http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.44.echo.mp4), etc. 

Once I had downloaded all the files, I also saved them in my student folder at [IAAC’s cloud](https://cloud.iaac.net:5001/index.cgi). 


![](img/electronics-design/getting-to-know-the-echo-hello-world-board/getting-to-know-the-echo-hello-world-board-02.png)


![](img/electronics-design/getting-to-know-the-echo-hello-world-board/getting-to-know-the-echo-hello-world-board-03.png)


!!! Info
    Note that the aforementioned files where not available in the [Fab Academy’s "Electronics Design" page](http://academy.cba.mit.edu/classes/electronics_design/index.html), but that there was a link under "**assignment**" at the end of that page that redirected me to the [Fab Academy’s “Embedded Programming” page](http://academy.cba.mit.edu/classes/embedded_programming/index.html#echo), where these PNG images (plus other programming files) for the EHW board were available.



## **Redrawing the EHW board**

I would have loved to use more than one of the EDA software packages presented during the lecture for this assignment, but as a newbie in electronics design I guessed that it would have taken me much more time than desired to learn how to use them, so I decided to use only one. 

Although other EDA software packages such as [Autodesk Circuits](https://circuits.io/) or [KiCad](http://kicad-pcb.org/) were also very appealing to me (and I will definitely practice with them as soon as I have time to do it), the software chosen to complete this part of the assignment was [Autodesk EAGLE](https://www.autodesk.com/products/eagle/overview).


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-01.png)


At the time I was completing this part of the assignment, there was a (restricted) [free version](https://www.autodesk.com/products/eagle/free-download) of EAGLE for hobbyists and makers that did not wanted to subscribe for the Standard version. 

DIY electronics giants like [Arduino](https://www.arduino.cc/), [Sparkfun](https://www.sparkfun.com/) and [Adafruit](https://www.adafruit.com/) had part libraries available. 

And there was also a [Fab Library (fab.lbr)](https://github.com/Academany/FabAcademany-Resources/blob/master/files/fab.lbr) maintained by the Fab Network, which turned out to be essential in order to complete this part of the assignment smoothly. 

Besides, the amount of EAGLE tutorials for beginners available online was huge, making this EDA software tool ideal for someone with zero experience like me.  



#### **Installing EAGLE** 

Following the ["Introduction to EAGLE"](http://docs.academany.org/FabAcademy-Tutorials/_book/en/week6_electronic_design/eagle_english.html) tutorial available at [docs.academany.org](http://docs.academany.org/FabAcademy-Tutorials/_book/en/), I downloaded the latest [Mac OS X installer](https://www.autodesk.com/eagle-download-mac) available at that moment, and I followed the on-screen installation instructions, until the installation process had finished.


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-02.png)


Once the installation had been completed, I launched EAGLE. 

Note that as the free version of EAGLE that I had installed was a subscription-based cloud tool, it asked me to create an Autodesk account before I could start using the program. 

In any case, I already had an Autodesk account, so I just had to sign in and EAGLE’s control panel appeared on the screen as soon as my authentication details were validated.


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-03.png)


#### **Downloading the schematic**

According to the ["Introduction to EAGLE" tutorial], the next step was to download the EHW board schematic example, but the link for downloading the “hello.fdti.44 schematic” file in that tutorial was broken, so I had to search for it on-line. 

Fortunately, I rapidly found a [web page inside the docs.academany.org platform](http://docs.academany.org/FabAcademy-Tutorials/_book/en/week6_electronic_design/eagle_english/hello-ftdi-44.sch) were the content of the "hello.fdti.44.sch" file could be seen as plain XML text file. 

Once the aforementioned page was completely loaded, I right-clicked over it, I selected "Save as" from the drop-down menu, and I saved it on my computer as "hello.fdti.44.sch.xml".

Then, I used my OS' file manager properties to remove the XML extension, and leaving downloaded file's name as "hello.fdti.44.sch", which allowed me to open it with EAGLE.


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-04.png)


With the schematic example of the "Hello Echo" board opened in EAGLE, I could have [created a project](https://www.element14.com/community/message/125735/l/how-to-add-an-existing-schematic-to-a-project#125735) directly, but I felt much more tempted to start the schematic from scratch.

Either way, the next step was to download and install [Fab Lab Network’s EAGLE library](https://github.com/Academany/FabAcademany-Resources/blob/master/files/fab.lbr). 


#### **Installing the Fab library**

First of all, I downloaded it from [Fab Academy’s Electronics Design page](http://academy.cba.mit.edu/classes/electronics_design/index.html). 

Immediately after, I moved the downloaded library from the "Downloads" folder to the "lbr" folder inside the EAGLE installation directory (~ / Applications / EAGLE-8.0.2 / lbr), where it will be sharing space with the other dozen of libraries that EAGLE includes by default.

![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-05.png)


The next step was making sure EAGLE knew that I wanted to use the “fab.lbr” library.

In order to do that, I went back to EAGLE’s control panel, and I expanded the "Libraries" drop down menu available in the navigation area on the left-side of the screen program.


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-06.png)


Then, I searched for “fab.lbr” in the “Libraries” drop down menu. 

Once I found it, I right-clicked over it and selected the ”Use” option from the drop-down menu.


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-07.png)


That was it! The "fab" library was correctly activated, and I could start with my first PCB design.


#### **Creating a new project**

As far as I understood while reading [Sparkfun’s EAGLE tutorial](https://learn.sparkfun.com/tutorials/using-eagle-schematic) (an absolutely recommended reading), PCB design in EAGLE is a two-step process. I had to design the schematic for the "hello.fdti.44" board first, and then I had to layout a PCB based on that schematic. 

In any case, the very first thing that I needed to address before I could proceed with any of the aforementioned steps was to create a new project folder.

In order to do that, I had to use EAGLE’s control panel once again.

I started by expanding the "Projects" drop down menu in the control panel's navigation area, and then, I right-clicked over "eagle", and I selected "New Project" from the drop-down menu.


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-08.png)


Once the "New_Project" folder had been created, it was recommended to give a more descriptive name to it, so I changed the name given by default to “hello.fdti.44.aitor”.

In that "hello.fdti.44.aitor" folder, I was going to house both the schematic and board design files that I was about to create. And eventually, it could also house the [Gerber files](https://en.wikipedia.org/wiki/Gerber_format) created later. 


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-09.png)



#### **Designing the schematic**

With the project folder created (and properly named), it was time to start designing.

In order to lay out the schematic, I right-clicked over the “hello.fdti.44.aitor” folder, and I selected "New > Schematic" from the drop-down menu.


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-10.png)


As soon as I selected the "Schematic" option from the drop-down menu tree, a new blank window (also know as the schematic editor) popped up over the control panel.


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-11.png)


According to the [Sparkfun's EAGLE tutorial](https://learn.sparkfun.com/tutorials/using-eagle-schematic) that I was still following, the schematic design was also a two step process in which I had to start by adding all of the parts to the schematic sheet. And when all the parts had been added, I had to wire all those parts together. 


The Sparkfun's EAGLE tutorial also said that these 2 steps could be intermixed (adding a few parts, wiring a few parts, adding some more part, wiring some more parts etc)

Anyway, I though that it would be better to take advantage of the "hello.fdti.44 with LED and button" reference schematic available at Fab Academy’s ["Introduction to EAGLE"](http://docs.academany.org/FabAcademy-Tutorials/_book/en/week6_electronic_design/eagle_english.html) tutorial, and add the parts to the schematic all at once. 


In order to start adding parts to the schematic editor, I maximized the schematic editor window (not mandatory), and I typed "add" in the schematic sheet’s command line.


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-12.png)


Then, I pressed Enter on my keyboard, and the "Add" dialog window (also known as "library navigator") appeared, showing a list with the specific libraries installed in EAGLE (left side).


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-13.png)


I looked through the listed libraries until I found the “fab” library. 

Once I found it, I expanded the "fab” library’s accordion menu by clicking the arrow.


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-14.png)


From the “fab” library’s menu, I selected the "6MM_SWITCH” part.

The two windows on the upper right-side of the screen program were updated showing both the schematic symbol of the push-button and its package.


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-15.png)


Then, I clicked “OK” at the lower left-side corner of the library navigator, and the library navigator window closed taking me back to the schematic editor.

Back in  the schematic editor, the symbol of the push-button that I had just selected could be seen hovering around and following the mouse. 

In order to place it on the schematic sheet, I left-clicked the mouse once. 


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-16.png)


After placing the push-button on the schematic sheet, the add tool was still active and the program was assuming that I wanted to add another push-button to the schematic.

This was not the case, so I had to hit the escape (ESC) key once to get out of the "add-mode".

Hitting the ESC key took me back to the library navigator, where I had to repeat the very same process as many times as necessary until I had added all the other fab library parts that were necessary to complete the “hello.fdti.44 with LED and button” schematic:


- 1x ATTINY44SSU (Microcontroller)
- 1x AVRISPSMD (ISP header)
- 1x CAP-UAS1206FAB (Capacitor)
- 1x FTDI-SMD-HEADER (FTDI header)
- 1x LED1206FAB (Light Emitting Diode)
- 3x RES-US1206FAB (Resistor)
- 1x RESONATOR (Resonator)


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-17.png)


In addition to the fab library parts that I had already added to the schematic window, I also had to add parts from other libraries, as the “VCC” and “GND” symbols that I needed to complete the echo hell-world schematic were not available in the fab library.


As I had no idea where to find the “VCC” and “GND” symbols, I though that using the library navigator's search functionality explained in Sparkfun’s EAGLE tutorial would be really helpful. 

I introduced the term "VCC" in the "Search" field at the lower-left side of the library navigator, and two options appeared in the library navigator, inside the “supply1” and “supply2” libraries. 


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-18.png)


The right “VCC” symbol for my schematic was the one inside the “supply1” library, so I selected it and left-clicked the “OK” button at the lower left-side corner of the library navigator. 

Back in the schematic editor, I placed four “VCC” symbols on the schematic sheet.


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-19.png)


Then, I searched for “GND”. 

As with the previous search, two different “GND” symbols were available inside the library navigator’s “supply1” and “supply2” libraries. 


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-20.png)


The “GND” symbol under "supply1" was the right one, so I selected it and left-clicked "OK".

Then, I hit the escape key (ESC) twice. 

The first hit to got out from the add-mode and go back to the library navigator, as I had previously done after adding other symbols. 

And the second hit to close the library navigator and go back to the schematic editor.


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-21.png)


At that point, all the parts that I needed in order to lay out the schematic of the "hello.fdti.44 with LED and button” were placed in the schematic editor. 

Anyway, some of them were not oriented as they appeared in the reference schematic, so I had to use the “Rotate” command (also available in the left toolbar or under the Edit menu) in order to orient them as in the reference schematic.

After a little while using the “Rotate” tool, all the parts in the schematic were correctly oriented. 

It was time to save the work done so far and wire all those parts together! 


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-22.png)


Anyway, before I could start wiring the parts on the schematic editor, I also had to move some of them around so they were more or less placed like in the reference schematic, as this would make wiring them much more easier.

In order move the parts, I used the "Move" command. 

Once the "Move" tool was activated, I left-clicked over red (+) symbol of the parts that I wanted to pick up, and I left-clicked again when they were placed in the right spot.


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-23.png)


With all the parts for the "hello.fdti.44 with LED and button" inserted in the schematic editor, correctly oriented, and properly placed on the schematic sheet, I could finally start wiring them.

Following the instructions given in the "Using the NET Tool" section of Sparkfun’s EAGLE tutorial, I used the "Net" command to start joining all the parts together.

With the schematic of the "hello.fdti.44 with LED and button" as guide, I wired the capacitor, the LED, and the push-button to their corresponding resistor, "VCC" and "GND" symbols.  


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-24.png)


Then, I wired the ATtiny 44 micro controller symbol's pin "1" (VCC) and pin "14" (GND) to their corresponding "VCC" and "GND" symbols. 


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-25.png)


And pin "4" [(PCINT11/RESET/DW) PB3] to "VCC", through the corresponding resistor.


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-26.png)


Finally, pins "2" [(PCINT8/XTAL1/CLK) PB0] and "3" [(PCINT9/XTAL2) PB1] to the resonator. 

And the resonator to the last “GND” symbol available.


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-27.png)


At that point, all the connections that could be completed using the "net" tool were done, but there were still many other connections waiting to be made. 

Unfortunately, these remaining connections could not be "wired" using the "net" tool because they could not be cleanly route, meaning that I needed to find any other suitable way. 

As explained in the "Making Named, Labeled Net Stubs" section of Sparkfun’s EAGLE tutorial, the nets that could not cleanly route had to be wired using the "Name" and "Label" commands.

Thus, I started this second part of the wiring process by using the "net" tool again, but adding short, one-sided net to each of the FTDI header and the ISP header pins.


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-28.png)


Then, I also added one-sided nets to the microcontroller pins that had not been wired yet, except for pins "5" [(PCINT10/INT0/OC0A/CKOUT) PB2] and "11" [(PCINT2/AIN1/ADC2) PA2].


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-29.png)


And I also added a short, one-sided net to the resistor that was connected to the LED.


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-30.png)


Once all the one-sided nets were created, I used the "Name" command to name each of them.

With the "Name" tool active, I clicked the one-sided net connected to the "VVC" pin of the ISP header and a new dialog window (called "Name") opened. 


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-31.png)


I deleted the auto-generated name, and replaced it with the corresponding pin name (VCC).

And then, I clicked the "OK" button in the lower right-side corner of the "Name" dialog window.


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-32.png)


As soon as I clicked the "OK" button, the "Name" dialog window closed and a "warning" dialog window appeared asking if I wanted to connect the selected net to "VCC". 

As that was exactly what I wanted, I clicked "Yes".


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-33.png)


After that, I repeated the very same process with all the other pins of the ISP header. 

Note that the warning dialog did not appear when I was naming every pin on the ISP header. Actually, it only appeared when I named the "VCC" and the "GND" pins. 

Later, when I was naming the nets of the microcontroller and the FTDI header, I realized that this warning dialog only appears if there is another pin with that name in the schematic.

Anyway, after naming all the nets of the ISP header, I used the "Label" command (also available in the left toolbar or under the Edit menu) to add a text label to each of the ISP header pins. 

With the "Label" tool selected, I left-clicked on the "VCC" net, which spawned a piece of text with the name of the net. And I just had to left-click again on the "VCC" net to place the label down right on top of the net.


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-34.png)


Then, I repeated the "labeling" process with the rest of the pins of the ISP header.


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-35.png)


Once the ISP header was done, I used the reference schematic as guide to repeat the naming and labeling process with the nets of the FTDI header and the microcontroller.


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-36.png)


Then, I also named and labeled the net of the LED’s resistor. 


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-37.png)


Just before I was going to get over with this stage of the wiring process, I realized that the net of the button was also named and labeled in the reference schematic, so I completed this last part by repeating the very same process that I had already done with all the other parts.


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-38.png)


At that moment, the schematic layout was completed. Niceeeee! 

But before moving forward and creating the board, I followed the “Tips and Tricks” section of [Sparkfun’s EAGLE tutorial](https://learn.sparkfun.com/tutorials/using-eagle-schematic) in order to complete the values of the resistors and the capacitor. 

And I also verified that all the connections between parts were ok,  using the “Show” command.


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-39.png)


Everything seemed to be ok, so could move to the next stage and start with the board layout! 


#### **Laying out the board**

To create a board from the schematic, I followed [the 2nd part of Sparkfun’s EAGLE tutorial](https://learn.sparkfun.com/tutorials/using-eagle-board-layout). 

The schematic-to-board process that I had to follow in EAGLE was extensively explained in that tutorial, and it was a very good idea to read it before taking any action.

Once I finished reading the tutorial, the first thing I did was switching from the schematic editor to the related board, by clicking the "Generate/Switch to Board" button on the top toolbar (also available as "Switch to Board" under "File" in the main menu bar).

As soon as I clicked the "Generate/Switch to Board" button, a new dialog window (also known as "board editor") opened, and all the parts that I had previously added in the schematic sheet were in there but in a disorderly manner, so I needed to correctly place and route them.


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-40.png)


Using the “Move” and “Rotate” commands, I arranged all the parts in the lower left corner of the board editor's white frame, making my layout was as close as possible to the echo hello board layout showed in the [“Introduction to EAGLE” tutorial](http://docs.academany.org/FabAcademy-Tutorials/_book/en/week6_electronic_design/eagle_english.html) that I was still checking at every step.


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-41.png)


Once the parts were correctly placed and oriented in the board editor window, I needed to adjust the "dimension layer" (represented by the four thin dimension lines of the white frame). 

I started by erasing the four dimension lines that were in the board editor from the beginning.

Then, I activated the "Wire" command and selected "20 Dimension" in the layers' menu. 

And finally, I drew a smaller box around the parts.


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-42.png)


With the parts laid out and the dimension layer fixed, I was time to start routing some copper!

The first thing I did was to activate the "Route" command and check that the options available at the tool bar (bend style, radius, width, etc) for this tool were the right ones. 

Then, I started routing all the parts by left-clicking on the pins (where the airwires terminated).

Note that while making the routes, I had to activate the grid (View > Grid at the main menu bar), so I could have a precise control. And that I also had to made a few slight changes in the position of some of the parts, so there was enough space between two different signal traces.


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-43.png)


Once the board was completely routed, it was time to check if there was any error.

According to the Sparkfun’s EAGLE tutorial, the first check was to make myself completely sure that I was actually routed all of the nets in my schematic. 

In order to do that, I hit the "RATSNEST" icon at the bottom of the left-hand toolbar. 

Immediately after, I checked the bottom left status box, and the message in the bottom-left status box was "Ratsnest: Nothing to do!", meaning that I had made every route required.


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-44.png)


Once the "ratsnest" check was done, there was one more check that I had to do in order to find any possible error. This check was the "Design Rule heck" or DRC.
 

#### **Checking  the design rules**

As seen in the wikipedia, the design rules check ensures that the layout of the PCB satisfies a series of recommended parameters. In other words, it ensures that a PCB layout designed meets the manufacturer’s limitations, or the manufacturing process’ constrains.

In this case, I was going to use a Roland SRM-20 desktop machine and a 1/64" end mill, so I needed to be sure that the separation between traces and component pads in my hello.fdti.44 board was big enough as for the end mill to complete the tool path strategies without trouble.

I had two options in order to complete the design rule check (or DRC): 

1. Download the DRU file available in the fab Academy archives that included the right settings for layers, distances, clearance, etc; 

2. Edit the values directly in the corresponding tabs of the DRC dialog window.

I decided to use the second method, so I opened the DRC dialog window by clicking the “DRC” button in the left-hand tools bar, and I modified all the values in the "Clearance" tab.


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-45.png)


Once I had changed all the values in the "Clearance" tab to "15.625mil" (which was the equivalent in thousands of an inch to the 1/64" milling bit diamater), I pressed the "Check" button in the bottom right-hand corner of the DRC dialog window.

As soon as I clicked the "Check" button, the DRC dialog window closed and a new dialog window named “DRC Errors” opened. 

In this new dialog window, there were 4 overlap errors!


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-46.png)


These overlap errors were just routes going to the pads and they were not going to be relevant at the time of manufacturing the circuit board using the SRM-20 mini CNC machine. 

Therefore, I selected the four errors and approved all of them by clicking the "Approve" button at the bottom right-hand corner of the "DRC Errors" dialog window. 

  
![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-47.png)


Finally, I just had to close the "DRC Errors" dialog window.

I clicked the red “X” button at the upper left-side corner of the  dialog window, and my “hello.fdti.44 with LED and button” board's design was ready to be exported!


#### **Exporting the board design**

The first thing I did in order to export the board design that I had just completed, was opening the "Display" dialog window by left-clicking over "View" at the main menu bar, and selecting “Layer settings…” in the drop-down menu that appeared.


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-48.png)


Once the "Display" dialog window was open, I deselected all the layers.


Then I selected Top, Pads, and Vias, and clicked the "Apply" button at the bottom left-hand corner of the "Display" dialog window. 

As soon as I clicked "Apply", all the layers that I had deselected in the "Display" dialog window disappeared from the board editor, leaving only visible the board's traces and pads.


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-49.png)


Next, I clicked the "OK" button at the bottom right-hand corner of the "Display" dialog window, and the electronic design was completely ready to be exported as an image. 

In order to do that, I clicked "File" at the main menu bar, and I selected "Export > Image" in the drop-down menu that popped-up. 

This opened the "Export file" dialog window.


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-50.png)


In the "Export file" dialog window, I clicked the "Browse" button in order to select the folder were I was going to save the exported file of my “hello.fdti.44 with LED and button” board.

Then, I selected "Monochrome", I changed the "Resolution" value to 500 dpi, and I double-checked that "Full" was selected under "Area".


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-51.png)


Finally, I clicked "OK" at the bottom right-hand corner of the "Export file" dialog window. 

The dialog window closed, and the PNG file was created in the corresponding directory.


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-52.png)


#### **Editing the exported file**

Once the PNG file with the traces had been exported, I needed to prepare it for Fab Modules.

This involved some raster graphics editing, so opened the PNG file I had exported using GIMP. 

Then, I added a 20px border around the image via "Image > [Canvas Size](https://docs.gimp.org/en/gimp-image-resize.html)".

And finally, I exported it as PNG file.


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-53.png)


I already had the PNG file for the traces, and I could prepare the RML file, but I still needed to create the PNG file for the PCB’s outline.

In order to do that, I just had to edit the image of the traces that is was still open. 

I filled the traces in black using the [Fill with BG Color](https://docs.gimp.org/en/gimp-edit-fill-bg.html) tool, I inverted the colors via "Colors > [Invert](https://docs.gimp.org/2.6/en/gimp-layer-invert.html)", and I exported the image as PNG being careful of changing the file name.


![](img/electronics-design/redrawing-the-echo-hello-world-board/redrawing-the-echo-hello-world-board-54.png)



## **Making the EHW board**

Once I had the two PNG files for the “hello.fdti.44 with LED and button” board that I had designed in EAGLE, I just needed to replicate all the steps that I had previously followed for manufacturing the hello.ISP.44 PCB during the Electronics Production week assignment. 


### **Milling the PCB**

First, I prepared the RML files using Fab Modules.

Then, I milled the board in the Roland SRM-20 mini CNC machine using the 1/64" and 1/32" milling bits, and taking into account all the tips and tricks that I had learned before. 

After a few minutes, my "hello.fdti.44 with LED and button" PCB was finished, and ready to have the electronic components solder onto it.


![](img/electronics-design/making-the-echo-hello-world-board/making-the-echo-hello-world-board-01.png)


### **Soldering the parts**

As I did when I had to assemble my hello.ISP.44 board, I started by identifying and collecting all the electronic components needed for the "hello.fdti.44 with LED and button" board. 

Taking advantage of the work that I had done when I was drawing the schematic in EAGLE, I made a list indicating the name and the quantity that I needed of each component:


- 1x AVR ATTiny 44 Microcontroller IC 8-Bit 20MHz 4KB (2K x 16) FLASH 14-SOIC
- 1x 6 Positions ISP Header Connector 0.100" (2.54mm) Surface Mount
- 1x Positions Header Breakaway Connector 0.100" (2.54mm) Surface Mount, Right Angle
- 1x 20MHz Ceramic Resonator Surface Mount
- 1x 1µF ±10% 50V Ceramic Capacitor X7R 1206 SMD
- 1x 499 Ohm 1/4W 1% Resistor 1206 SM
- 2x 10k Ohm 1/4W 1% Resistor 1206 SMD
- 1x LED SMD 1206
- 1x SMD Switch Tactile SPST-NO 0.05A 24V

Then, I searched for their [Digi-Key](https://en.wikipedia.org/wiki/Digi-Key) Part Number in the [Fab Lab Inventory](https://docs.google.com/spreadsheets/d/1U-jcBWOJEjBT5A0N84IUubtcHKMEMtndQPLCkZCkVsU/pub?single=true&gid=0&output=html), and double-checked the product pages for each of these parts:

- ATTiny 44 microcontroller → [Digi-key Part Number: ATTINY44A-SSU-ND](https://www.digikey.com/products/en?keywords=ATTINY44A-SSU-ND%09)
- 6 pin ISP header → [Digi-key Part Number: 609-5161-1-ND](https://www.digikey.com/products/en?keywords=609-5161-1-ND%09)
- 6 pin FTDI header → [Digi-key Part Number: S1143E-36-ND](https://www.digikey.com/products/en?keywords=S1143E-36-ND)
- 20MHz resonator → [Digi-key Part Number: XC1109CT-ND](https://www.digikey.com/products/en?keywords=XC1109CT-ND%09)
- 1uF capacitor →  [Digi-key Part Number: 445-1423-1-ND](https://www.digikey.com/products/en?keywords=445-1423-1-ND)
- 499 Ohm resistor → [Digi-key Part Number: 311-499FRCT-ND](https://www.digikey.com/products/en?keywords=311-499FRCT-ND%09)
- 10k Ohm resistor → [Digi-key Part Number: 311-10.0KFRCT-ND](https://www.digikey.com/products/en?keywords=311-10.0KFRCT-ND%09)
- SMD LED → [Digi-key Part Number: 160-1167-1-ND](https://www.digikey.com/products/en?keywords=160-1167-1-ND)
- SMD switch → [Digi-key Part Number: SW262CT-ND](https://www.digikey.com/products/en?keywords=SW262CT-ND%09)

Note that I had already used some of these parts when I assembled the hello.ISP.44 board, but others were totally new, so I found the information available in the product pages (and data sheets) of these new parts pretty interesting.


![](img/electronics-design/making-the-echo-hello-world-board/making-the-echo-hello-world-board-02.png)


Finally, I used the aforementioned parts list for collecting the electronic components, and I went back home in order to assemble the board in the tranquility of my desk.


![](img/electronics-design/making-the-echo-hello-world-board/making-the-echo-hello-world-board-03.png)


  

## **Testing the EHW board**

Once I had my hello.ftdi.44 board assembled, it was time to test it.

To program the hello.ftdi.44, I needed the [hello.ftdi.44.echo.c](http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.44.echo.c) and [hello.ftdi.44.echo.c.make](http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.44.echo.c.make) files available under "**host communication**" in the [Embedded Programing page](http://academy.cba.mit.edu/classes/embedded_programming/index.html#echo) 

I had already downloaded and saved these 2 files at the beginning of the assignment, so I just had to copy them to a local directory that I had created for programming purposes.

Besides the aforementioned files, I also needed:

- The FabISP that I had set as programmer during the Electronics Production week 

- The IDC ISP cable that I had also made during the Electronics Production week 

- A [FTDI Serial TTL-232 USB cable](http://www.ftdichip.com/Products/Cables/USBTTLSerial.htm)

- A USB-A to mini-USB cable.


![](img/electronics-design/testing-the-echo-hello-world-board/testing-the-echo-hello-world-board-01.png)



### **Powering the boards**

First, I connected the FabISP programmer to my laptop using the USB-A to mini-USB cable. 


![](img/electronics-design/testing-the-echo-hello-world-board/testing-the-echo-hello-world-board-02.png)


Then, I connected the hello.ftdi.44 board to my laptop using the FTDI Serial TTL232 USB cable. 


![](img/electronics-design/testing-the-echo-hello-world-board/testing-the-echo-hello-world-board-03.png)


Finally, I connected the two boards together using the ISP ribbon cable.

!!! Important
    The connection between ISP headers of the FabISO and the hello.ftdi.44 had to also "straight-through", so I double-checked that the IDC ISP cable was connecting the headers of the two boards correctly.


![](img/electronics-design/testing-the-echo-hello-world-board/testing-the-echo-hello-world-board-04.png)



### **Checking the system profiler**

With all the connections done, I thought that verifying that both devices had been recognized by my computer would a good idea before doing anything else. 

Therefore, I opened my machine’s System Profiler. 

In the USB devices three I was able to see a “FabISP” device and a “TTL232R” device, meaning that I could move on.


![](img/electronics-design/testing-the-echo-hello-world-board/testing-the-echo-hello-world-board-05.png)



### **Compiling the program files**

Once the connections were done and the two devices connected, the next step was opening the Terminal program, and navigating to the directory were I had saved both the [hello.ftdi.44.echo.c](http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.44.echo.c) and the [hello.ftdi.44.echo.c.make](http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.44.echo.c.make) files:

    $ cd  Desktop/FabAcademy/electronics-design/hello.ftdi.44/programing/ 

Once in the right directory, I typed:

    $ make -f hello.ftdi.44.echo.c.make 

Which prompted in the console:

    avr-objcopy -O ihex hello.ftdi.44.echo.out hello.ftdi.44.echo.c.hex;\
    avr-size --mcu=attiny44 --format=avr hello.ftdi.44.echo.out
    AVR Memory Usage
    ----------------
    Device: attiny44
    
    Program:     758 bytes (18.5% Full)
    (.text + .data + .bootloader)
    
    Data:         64 bytes (25.0% Full)
    (.data + .bss + .noinit)

Meaning that the corresponding executable (hello.ftdi.44.echo.out) and hexadecimal (hello.ftdi.44.echo.c.hex) files that I needed to program the board later had been generated.

Note that these two files had been created in the exact same directory where the [hello.ftdi.44.echo.c](http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.44.echo.c) and [hello.ftdi.44.echo.c.make](http://academy.cba.mit.edu/classes/embedded_programming/hello.ftdi.44.echo.c.make) files were located.


![](img/electronics-design/testing-the-echo-hello-world-board/testing-the-echo-hello-world-board-06.png)


### **Setting the fuses**

Once the executable and hexadecimal files had been created, it was time set the fuses.

Therefore, I typed:

    $ make -f hello.ftdi.44.echo.c.make program-usbtiny-fuses

And the console prompted:

    avr-objcopy -O ihex hello.ftdi.44.echo.out hello.ftdi.44.echo.c.hex;\
    avr-size --mcu=attiny44 --format=avr hello.ftdi.44.echo.out
    AVR Memory Usage
    ----------------
    Device: attiny44
    
    Program:     758 bytes (18.5% Full)
    (.text + .data + .bootloader)
    
    Data:         64 bytes (25.0% Full)
    (.data + .bss + .noinit)
    
    avrdude -p t44 -P usb -c usbtiny -U lfuse:w:0x5E:m
    
    avrdude: AVR device initialized and ready to accept instructions
    
    Reading | ################################################## | 100% 0.01s
    
    avrdude: Device signature = 0x1e9207
    avrdude: reading input file "0x5E"
    avrdude: writing lfuse (1 bytes):
    
    Writing | ################################################## | 100% 0.01s
    
    avrdude: 1 bytes of lfuse written
    avrdude: verifying lfuse memory against 0x5E:
    avrdude: load data lfuse data from input file 0x5E:
    avrdude: input file 0x5E contains 1 bytes
    avrdude: reading on-chip lfuse data:
    
    Reading | ################################################## | 100% 0.00s
    
    avrdude: verifying ...
    avrdude: 1 bytes of lfuse verified
    
    avrdude: safemode: Fuses OK (H:FF, E:DF, L:5E)
    
    avrdude done.  Thank you.

Meaning that the fuses had been correctly set.


### **Flashing the EHW board**

Finally, once the fuses had been set, it was time to program the board. So, I typed:

    $ make -f hello.ftdi.44.echo.c.make program-usbtiny 

And the console prompted:

    avr-objcopy -O ihex hello.ftdi.44.echo.out hello.ftdi.44.echo.c.hex;\
    avr-size --mcu=attiny44 --format=avr hello.ftdi.44.echo.out
    AVR Memory Usage
    ----------------
    Device: attiny44
    
    Program:     758 bytes (18.5% Full)
    (.text + .data + .bootloader)
    
    Data:         64 bytes (25.0% Full)
    (.data + .bss + .noinit)
    
    avrdude -p t44 -P usb -c usbtiny -U flash:w:hello.ftdi.44.echo.c.hex
    
    avrdude: AVR device initialized and ready to accept instructions
    
    Reading | ################################################## | 100% 0.01s
    
    avrdude: Device signature = 0x1e9207
    avrdude: NOTE: "flash" memory has been specified, an erase cycle will be performed
             To disable this feature, specify the -D option.
    avrdude: erasing chip
    avrdude: reading input file "hello.ftdi.44.echo.c.hex"
    avrdude: input file hello.ftdi.44.echo.c.hex auto detected as Intel Hex
    avrdude: writing flash (758 bytes):
    
    Writing | ################################################## | 100% 1.21s
    
    avrdude: 758 bytes of flash written
    avrdude: verifying flash memory against hello.ftdi.44.echo.c.hex:
    avrdude: load data flash data from input file hello.ftdi.44.echo.c.hex:
    avrdude: input file hello.ftdi.44.echo.c.hex auto detected as Intel Hex
    avrdude: input file hello.ftdi.44.echo.c.hex contains 758 bytes
    avrdude: reading on-chip flash data:
    
    Reading | ################################################## | 100% 1.60s
    
    avrdude: verifying ...
    avrdude: 758 bytes of flash verified
    
    avrdude: safemode: Fuses OK (H:FF, E:DF, L:FE)
    
    avrdude done.  Thank you.

Meaning that my “hello.fdti.44 with LED and button” board had been correctly programmed, and that everything should be working fine. 

Anyway, I could not be totally sure without testing it.


### **Communicating with the board**

One of the multiple ways to communicate via serial with my hello.fdti.44 was using term.py.

[Term.py](http://fab.cba.mit.edu/about/fab/hello/python/term.py) is a little [Python](https://www.python.org/) program written by Neil that can be launched in order to connect a computer and a board via [serial](https://en.wikipedia.org/wiki/Serial_communication), allowing bidirectional communication. 

I downloaded the term.py program from the [Fab Academy’s Embedded Programing assignment page](http://academy.cba.mit.edu/classes/embedded_programming/index.html#echo) (under "host communication"), and I saved it in the very same directory that I had used for programming the “hello.fdti.44 with LED and button” board a few of minutes before. 

Note that in order to launch the term.py program, I needed to have Python installed in my computer so the next step should have been downloading and installing Python. 

Anyway, macOS does include a Python version by default. This Python version is not the latest, but it seemed to be also suitable for launching the term.py program.

First, I went back to the terminal window and I typed:

    $ python --version

Which prompted:

    Python 2.7.10

Meaning the I actually had a copy of Python v2.7.10 installed in my computer.

The next thing I did was checking the USB port were the EHW board was connected.

In order to do that, I typed:

    $ ls /dev/cu.*

Wich prompted:

    /dev/tty.Bluetooth-Incoming-Port /dev/tty.usbserial-FTEZU7NB

Meaning that the echo hello-world board was connected to ```/dev/tty.usbserial-FTEZU7NB```.

At that point, I just had to type:

    $ python term.py  /dev/tty.usbserial-FTEZU7NB 115200

As soon as I hit enter, a new dialog window called "term.py" opened:


![](img/electronics-design/testing-the-echo-hello-world-board/testing-the-echo-hello-world-board-07.png)


Then, I started to type "Hello World!", and the "term.py" dialog window prompted a response message with every key stroke.

This confirmed that the echo hello-world board had been correctly programmed.


![](img/electronics-design/testing-the-echo-hello-world-board/testing-the-echo-hello-world-board-08.gif)




## **Completing the group project**


### **Using the power supply**

what is a multimeter!?


use the test equipment in your lab to observe the operation of a microcontroller circuit board


### **Using the multimeter**

what is a multimeter!?


### **Using the osciloscope**

what is a osciloscope!?







## **What I have learned**

reading an electronic schematic

designing an electronic board with eagle

using term.py 


## **Issues that I had**

changing EAGLE version while working on a project

 


## **Tools used**

- **For redrawing the echo hello-world board:**
    - MacBook Pro with macOS High Sierra v10.13.3
    - EAGLE v8.0.2
    - GIMP v2.8.22


- **For making the echo hello-world board:**
    - Milling 
        - Roland SRM-20
        - Hex key
        - FR1 PCBs
        - 1/64” SE 2FL milling bit 
        - 1/32” SE 2FL milling bit
        - Double-sided tape
        - Spatula
        - Paper towels
        - Rubbing alcohol
        - Steel wool
    - Assembling
        - Soldering iron with a 0.5mm conical fine-point tip
        - Soldering iron stand with sponge
        - Distilled water
        - 0.35 mm soldering wire
        - Desoldering wick
        - Tweezers
        - Vacuum base PanaVise PCB holder
        - Solder fume extractor
        - Magnifying glass
    
- **For Testing the Hello Echo-World board:**
    - MacOS High Sierra v10.13.3
    - Terminal v2.8.2 (404)
    - Python v2.7.10
    - Neil's term.py program
    - FabISP programmer
    - USB-A to mini USB cable
    - FTDI Serial TTL-232 USB cable
    - 6 wire IDC ribbon cable
    - Multimeter
    - Osciloscope
    - Power supply



## **Files to download**

