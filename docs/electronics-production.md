# Electronics Production

As individual project for this week's assignment I had to fabricate a FabISP programmer.

On the other hand, as group project we had to describe the PCB production process and double-check that the desktop CNC machines available at the lab were working well.





## **Which FabISP to make**

A FabISP is an in-system programmer for AVR micro-controllers, designed for production within a Fab Lab that allows to program the micro-controllers on other boards.

Fabricate one of these programmers meant [milling](https://bit.ly/2GNcs09) the corresponding [printed circuit board (PCB)](https://en.wikipedia.org/wiki/Printed_circuit_board), [soldering](https://en.wikipedia.org/wiki/Soldering) the [electronic components](https://en.wikipedia.org/wiki/Electronic_component), [programing](https://en.wikipedia.org/wiki/Computer_programming) the assembled board, and testing it. 

Among the several versions of the FabISP available in [Fab Academy’s Electronics Production page](http://academy.cba.mit.edu/classes/electronics_production/index.html), we could choose any of them for the completion of this assignment.

In my particular case, I would have loved to make more than one version, but I finally opted for one of Neil’s versions (which was based on David Mellis's [FabISP](http://fab.cba.mit.edu/content/projects/fabisp/), which was based on Limor's [USBTinyISP](http://www.ladyada.net/make/usbtinyisp/index.html), which was based on Dick Streefland's [USBTiny](http://dicks.home.xs4all.nl/avr/usbtiny/)).

Moreover, from the two board alternatives available of Neil’s version (hello.ISP.44 vs hello.ISP.44.res), I decided to go for the hello.ISP.44 board. 

!!! Info
    The main difference between the hello.ISP.44 board and the the hello.ISP.44.res board is that the hello.ISP.44 board uses a 20MHz crystal oscillator and two 10pF capacitors for the clock signal, while the hello.ISP.44.res board uses just one 20MHz resonator. Apart from that, everything else was completely the same!


![](img/electronics-production/deciding-which-fabisp-to-make/deciding-which-fabisp-to-make-01.png)


The very first thing I did after deciding which FabISP to make, was to download all the PNG files related to the hello.ISP.44 board available at [Fab Academy’s "Electronics Production" page](http://academy.cba.mit.edu/classes/electronics_production/index.html) ([board](http://academy.cba.mit.edu/classes/embedded_programming/hello.ISP.44.png), [components](http://academy.cba.mit.edu/classes/embedded_programming/hello.ISP.44.components.png), [traces](http://academy.cba.mit.edu/classes/embedded_programming/hello.ISP.44.traces.png), and [interior](http://academy.cba.mit.edu/classes/embedded_programming/hello.ISP.44.interior.png)).  

Then, I saved all these files in my Fab Academy student folder at [IAAC’s cloud](https://cloud.iaac.net:5001/index.cgi). 

!!! Tip
    As I was going to need the [traces](http://academy.cba.mit.edu/classes/embedded_programming/hello.ISP.44.traces.png) and [interior](http://academy.cba.mit.edu/classes/embedded_programming/hello.ISP.44.interior.png) PNG files for preparing the CNC milling files first, and then I was going to need  the [board](http://academy.cba.mit.edu/classes/embedded_programming/hello.ISP.44.png) and [components](http://academy.cba.mit.edu/classes/embedded_programming/hello.ISP.44.components.png) PNG files for pinpointing the parts and assembling the board, to start getting used to save the files in the cloud seemed like a good idea to me. This would allow me to quickly access them from any of the computers that I would have to use during the whole PCB manufacturing process.


![](img/electronics-production/deciding-which-fabisp-to-make/deciding-which-fabisp-to-make-02.png)


![](img/electronics-production/deciding-which-fabisp-to-make/deciding-which-fabisp-to-make-03.png)


Once I had downloaded the PNG files, I could have moved to generate the milling files, but as these milling files had to be prepared taking into account the machine and tools to be used, I though that it would be much more interesting to gather further information about them first.





## **Reviewing the PCB milling machines**

Needless to say, learning how to properly use the desktop PCB prototyping machines available at the lab was essential in order to complete this assignment successfully. 

On the other hand, I already knew that we were going to use these little CNC milling machines in subsequent assignments ([Electronics Design](http://academy.cba.mit.edu/classes/electronics_design/index.html), [Molding and Casting](http://academy.cba.mit.edu/classes/molding_casting/index.html), [Input Devices](http://academy.cba.mit.edu/classes/input_devices/index.html), [Output Devices](http://academy.cba.mit.edu/classes/output_devices/index.html), and [Networking and Communications](http://academy.cba.mit.edu/classes/networking_communications/index.html)), so it seemed really convenient to learn as much as possible about how these machines work from the very beginning.

At the time I was writing this review, there were two different models of desktop CNC machines available at Fab Lab Barcelona’s electronics room:



### [**Modela MDX-20**](http://support.rolanddga.com/_layouts/rolanddga/productdetail.aspx?pm=mdx-20)


![](img/electronics-production/reviewing-the-pcb-milling-machines/reviewing-the-pcb-milling-machines-01.png)


The MDX-20 was the oldest of the PCB prototyping mini CNC machines available at the lab. 

Nonetheless, it was not only capable of milling material such as ABS, acrylic, plaster, styrene foam, chemical wood, modeling wax, and light metals such as aluminum and brass, but it could also be used for precise 3D scanning using the “Roland Active Piezo Sensor” accessory.

The maximum work area for this machine was 203,2 (X) x 152,4 (Y) x 60,5 (Z) mm. And while the software resolution was 0.025 mm/step, the mechanical resolution was 0,00625mm/step. 

On the other hand, the feed rate during milling operations could go from 0.1 to 15 mm/sec.

It included a 10W DC spindle motor that could reach a maximum revolution speed of 6500 rpm, with a tool chuck for 3.175 mm (1/8") shank diameter [milling bits](https://en.wikipedia.org/wiki/Milling_cutter).

The machine was connected via RS-232C (using a Serial to USB adapter) to a dual-boot Windows7/Ubuntu computer. And as far I could find out, it used to be run on Windows for 3D scanning operations (using Dr. PICZA software application), and for milling jobs, it was operated using the backend installation of [Fab Modules](http://kokompe.cba.mit.edu/) in Ubuntu.


!!! Info
    At that time, this desktop CNC machine had been already discontinued and replaced by the MonoFab SRM-20.



### [**MonoFab SRM-20**](https://www.rolanddgi.com/en-es/products/modelling-machines/monofab-srm-20) 


![](img/electronics-production/reviewing-the-pcb-milling-machines/reviewing-the-pcb-milling-machines-02.png)


The monoFab SRM-20 was the latest desktop CNC milling machine by Roland back then. 

As mentioned above, one might said that it was an updated version of the Modela MDX-20, although this new version was intended for CNC milling only (modeling wax, wood, chemical wood, foam, cork, plaster, acrylic, poly-acetate, ABS, PCBs), and did not include the 3D scanning capabilities of its predecessor. 

This machine's operation strokes were also 203,2 (X) x 152,4 (Y) x 60,5 (Z) mm, but the spindle motor and the tool chuck system were completely different. 

It incorporated a DC Motor Type 380 with a maximum rotation speed of 7000 rpm, and the collet system for 1/8" shank diameter milling bits included as standard was independent, so it could be easily replaced by another collet for cutting tools with bigger or smaller shank diameters.

The software resolution was variable depending on the control commands set used: 0.01 mm/step for RML-1 commands, and 0.001 mm/step for NC code commands.  

On the other hand, the maximum mechanical resolution was 0.000998594 mm/step.

Unlike the Modela MDX-20, the monoFab SRM-20 machines had a fully enclosed cabinet, reducing dust and noise. The cover also featured an intelligent safety interlock that automatically paused the machine when the cover was opened. 

Each of the 2 monoFab SRM-20 machines available at Fab Lab Barcelona was connected via USB to a dedicated dual-boot Windows7/Ubuntu computer.

Under windows, the software installed was VPanel (control panel),  MODELA Player 4 (CAM), Virtual MODELA (preview tool), iModela Creator (design to production tool), ClickMILL software (direct control), and SFEdit2 software (TrueType to Stroke font converter).





## **Getting to know the tools**

It would have been great to learn how to use both models of desktop CNC milling machines during this assignment, but the computer connected to the Modela MDX-20 was not booting at that moment, so I basically focused on thoroughly learn how to use the monoFab SRM-20. 

Maybe in the future (if the computer attached to the Modela MDX-20 is fixed), I would have the chance to spend some time playing with it. I would really love that!



### **Reading the user manual**

Reading the 152 pages of the [monoFab SRM-20 user manual](http://support.rolanddga.com/docs/documents/departments/technical%20services/manuals%20and%20guides/srm-20_use_en.pdf) gave me a general idea of the very basics when working with a desktop milling machine, as it described in detail the machine’s features, including part names and functions.

The user manual also explained the workflows when using the different software applications provided by Roland (SRP Player, MODELA Player, etc).

As I was going to use Roland’s VPanel software application and the web-browser version of [Fab Modules](http://kokompe.cba.mit.edu/) for milling the PCB, most of the software related information in the user manual was not very relevant initially, but surely it would be very useful later.


![](img/electronics-production/getting-to-know-the-tools/getting-to-know-the-tools-01.png)



### **Checking the stock material**

The stock material that we were going to use, among all the stock materials that could be used to make PCBs with the monoFab SRM-20, was phenolic paper or [FR-1.](https://support.bantamtools.com/hc/en-us/articles/115001671734-FR-1-PCB-Blanks-) 

FR1 is a material often used to make printed circuit board substrates, that is made of [wood fiber](https://en.wikipedia.org/wiki/Wood_fibre) and [phenolic polymers](https://en.wikipedia.org/wiki/Phenol_formaldehyde_resin). It is a fiber reinforced plastic, most commonly brown in color, and can have a copper foil lamination on one or both sides. 

In my particular case, the circuit board stock that I was going to use for making the hello.ISP.44 board was a 1.6 mm thick 3x2" (~76x51 mm) single sided FR1 PCB. 

!!! Info
    With one FR1 PCBs it could be possible to mill up to three hello.ISP.44, as long as the space was well seized.


![](img/electronics-production/getting-to-know-the-tools/getting-to-know-the-tools-02.png)



### **Reviewing the cutting tools**

The cutting tools were a very relevant element to have in mind when generating the milling files.

In that sense, the monofab's user manual included a quick view of the different shapes of milling bits that could be used (flat tip, round tip, pointed tip, etc). And it also provided an extremely important information: how to properly attach these cutting tools to the existing collet.

As explained by Neil during the lecture, among the vast amount of milling bits that could be used with this machine, we were going to use mainly two bits for machining the PCBs. A [1/64" 2FL (2 flute) flat-end bit](http://www.carbidedepot.com/00156in-DIA-2FL-SE-AlTiN-164-P180142.aspx) for the traces, and a [1/32" 2FL flat-end bit](http://www.carbidedepot.com/00312in-DIA-2FL-SE-AlTiN-132-P180143.aspx) for the outcuts. 

!!! Info
    A [third 0.01" 2FL flat-end milling bit](http://www.carbidedepot.com/00100in-DIA-220M-010-P70452.aspx) was also inventoried for milling PCBs, but only when the distance between the traces/pads of the circuit was so small that we were forced to use this extra-tiny milling bit instead of the 1/64" bit.


![](img/electronics-production/getting-to-know-the-tools/getting-to-know-the-tools-03.png)


Once I had learned about the machine, the workflow, the stock material, and the milling bits, it was time to move on to the next stages.



## **Generating the milling files**

[PNG](https://en.wikipedia.org/wiki/Portable_Network_Graphics) is a very useful file extension to store graphics on websites but, unfortunately, it was not a file extension that the monoFab SRM-20 milling machines could understand.

In order to make the monoFab SRM-20 to understand the images of the circuit that I had downloaded in PNG file format, it was necessary to convert them to G-code.



### **Understanding G-Code**

[G-code](https://en.wikipedia.org/wiki/G-code) is the most widely used programming language for controlling industrial machines such as [mills](https://en.wikipedia.org/wiki/Milling_(machining)), [lathes](https://en.wikipedia.org/wiki/Lathe) and [cutters](https://en.wikipedia.org/wiki/Plasma_cutting), as well as [3D-printers](https://en.wikipedia.org/wiki/3D_printing). 

It has many variants, but most (or all) adhere to certain common rules. 

The monoFab SRM-20 milling machine, in particular, interprets both [RML code](https://itp.nyu.edu/classes/cdp-spring2014/files/2014/09/RML1_Manual.pdf) (Roland Machine Language), as well as the industry standard [NC code](http://support.rolanddga.com/docs/documents/departments/technical%20services/downloads/mdx-nc_code_en.pdf) (G-code).



### **Learning Fab Modules**

Fab Modules is a browser-based CAM system, which allows to generate toolpaths for and control lasercutters, CNC-mills and waterjets commonly found in Fab Labs.

It can be used to easily generate RML code, so using Fab Modules would be one of the right ways to go for this exercise (although using alternative software was also tempting). 

Besides, learning how to use the [browser-based version](http://fabmodules.org/) of Fab Modules is super-easy, and the system application allows access from any internet-connected computer, which made the RML files preparation very straightforward for me. 



### **RML code for the traces**

In order to start converting the PNG files to RML code using the web browser version of Fab Modules, the first thing I did was to open a new browser window in Google Chrome.

!!! Info
    Note that Mozilla Firefox is another great web-browser to use Fab Modules.

Then, I wrote "fabmodules.org" in the address bar.


![](img/electronics-production/generating-the-milling-files/generating-the-milling-files-01.png)


Once the "fabmodules.org" page was loaded, I left-clicked the "input format" button at the upper left-hand corner of the web page, and I selected "image (.png)" from the drop-down list.


![](img/electronics-production/generating-the-milling-files/generating-the-milling-files-02.png)


As soon as I selected "image (.png)", a navigation window opened, and I used that navigation window to search for the "hello.ISP.44.traces.png" file.

Once I found the corresponding file, I opened it and the image with the traces of the hello.ISP.44 PCB appeared on the screen. 


![](img/electronics-production/generating-the-milling-files/generating-the-milling-files-03.png)


Simultaneously, a new button labeled as "output format" appeared right next to the former "input format" button (which had been converted to "image (.png)" after selecting that option).


![](img/electronics-production/generating-the-milling-files/generating-the-milling-files-04.png)


On the other hand, an "input" parameters menu appeared at the upper right-hand side of the fabmodules.org web page with details of the opened file (name, dpi, size).


![](img/electronics-production/generating-the-milling-files/generating-the-milling-files-05.png)


Just in case, I checked the size and resolution of the board. Everything seemed to be OK!

Next, I left-clicked the "output format" button, and I selected "Roland mill (.rml)" from the drop-down list that showed up.


![](img/electronics-production/generating-the-milling-files/generating-the-milling-files-06.png)


As soon as I selected the "Roland mill (.rml)", a new button labeled as "process" appeared right next to the former "output format" button (which had been converted to "Roland mill (.rml)").

Furthermore, an new parameters menu named "output" popped up just below the "input" parameters menu that had appeared in the previous step.


![](img/electronics-production/generating-the-milling-files/generating-the-milling-files-07.png)


Next, I clicked the "process" button, and I selected "PCB traces (1/64)" from the drop-down list. 

!!! Important
    Selecting the right milling bit for each process is crucial for Fab Modules to calculate the tool paths correctly.


![](img/electronics-production/generating-the-milling-files/generating-the-milling-files-08.png)


As it happened before, when I selected "PCB traces (1/64)" from the "process" drop-down menu, a new parameters menu named "process" appeared at the right-hand side of the screen.


![](img/electronics-production/generating-the-milling-files/generating-the-milling-files-09.png)


Once I had selected the right "input", "output" and "process" in the corresponding drop-down menus at the top of the fabmodules.org page, it was time to modify the values in the "output" and "process" parameters menus at the right-hand of the screen.

In order to continue with the RML files preparation, I started by revealing the drop-down menu in "output > machine" and selecting "SRM-20" on it. 

Selecting that option, it auto-filled the rest of parameters under "output" with default values. 

Note that I left the "speed (mm/s)" parameter as it appeared by default, but I changed the "x0 (mm)", "y0 (mm)", and "z0 (mm)" parameters to 0, so there would be no offset from the origin point that I was going to set in the machine later. 

In addition, I changed the "zjog (mm)" parameter to 12, so the tool would rise 12 mm over the PCB every time it had to travel from one point to the other during the milling process.

- machine: SRM-20
- speed (mm/s): 4
- x0 (mm): 0
- y0 (mm): 0
- x0(mm): 0
- zjog (mm): 12
- xhome (mm): 0
- yhome (mm): 152.4
- zhome (mm): 60.5


![](img/electronics-production/generating-the-milling-files/generating-the-milling-files-10.png)


Unlike before, I did not change any of the values in the "process" parameters menu, leaving them with the settings by default:

- direction: climb  
- cut depth (mm): 0.1
- tool diameter (mm): 0.4
- number of offsets (-1 to fill): 4
- offset overlap (%): 50
- path error (pixels): 1.1
- image threshold (0-1): 0.5
- sort path:  yes
- sort merge diameter multiple: 1.5
- sort order weight: -1
- sort sequence weight: -1

Next, I left-clicked the "calculate" button available under "process". 

A few seconds later the cut paths started to show up one by one in blue color, up to a total number of 4 (as selected in the "number of offsets" parameter). 

!!! Info
    Along with the four blue cut paths, it was also possible to see the travel paths in red color.


![](img/electronics-production/generating-the-milling-files/generating-the-milling-files-11.png)


Finally, I left-clicked the "save" button under "process", and a new navigation window opened. 

I used that navigation window to search for my Fab Academy student folder at IAAC’s cloud, and once I found it, I saved the milling job as "hello.ISP.44.traces.rml".

The RML file for machining the traces of the hello.ISP.44 PCB was ready, but I still needed to generate the RML for the outline cut.



### **RML code for the outline**

To prepare the RML file for the hello.ISP.44 PCB's outline cut, I basically repeated the very same process again, but changing the source file, some options, and few parameter values.

I started by refreshing Google Chrome (cmd+5), meaning that all the changes made while preparing the previous RML file disappeared from the window browser. 

I was back to the beginning, and the "input format" button was there again.


![](img/electronics-production/generating-the-milling-files/generating-the-milling-files-12.png)


I left-clicked the "input format" button and selected "image (.png)" from the drop-down menu.

Then, I searched for the "hello.ISP.44.interior.png" file using the navigation window that popped up, and I loaded it.

As happened before, the image with the outline of the hello.ISP.44 PCB appeared on the screen, the "Output format" button popped up right next to the former "input format" button, and the "input" parameters menu showed up at the upper right of the fabmodules.org page.


![](img/electronics-production/generating-the-milling-files/generating-the-milling-files-13.png)


Next, I clicked the "output format" button, and selected the "Roland mill (.rml)".

The "process" button popped up right next to the former "output format" button, and the "output" parameters menu showed up at the right side-hand of the web page. 

!!! Info
    The whole RML file preparation process was identical to the previous one so far (except for the PNG used). A few settings will be changed on next steps!


![](img/electronics-production/generating-the-milling-files/generating-the-milling-files-14.png)


Next, I clicked the "process" button, and I selected "PCB outline (1/32)" from the drop-down list, and the "process" parameters menu appeared at the right-hand side of the page.


![](img/electronics-production/generating-the-milling-files/generating-the-milling-files-15.png)


It was time to edit the "output" and "process" settings.

Under "Output > machine", I selected "SRM-20", and I changed the "speed (mm/s)" parameter to 0.5, the "x0(mm)", "y0(mm)", and "z0(mm)" parameters to 0; and the "zjog (mm)" parameter to 12.

- machine: SRM-20
- speed (mm/s): 0.5
- x0 (mm): 0
- y0 (mm): 0
- x0(mm): 0
- zjog (mm): 12
- xhome (mm): 0
- yhome (mm): 152.4
- zhome (mm): 60.5

!!! Info
    As I found out later, the default value of the "speed (mm/s)" parameter (which was 4) would also be suitable to cut the PCB's outline. Anyway, if you are not on a hurry to complete the milling job, it is always preferable to be a bit conservative.


![](img/electronics-production/generating-the-milling-files/generating-the-milling-files-16.png)


Next, I kept the default values of the "process" parameters:

- direction: climb  
- cut depth (mm): 0.6
- stock thickness (mm): 1.7
- tool diameter (mm): 0.79
- number of offsets (-1 to fill): 1
- offset overlap (%): 50
- path error (pixels): 1.1
- image threshold (0-1): 0.5
- sort path:  yes
- sort merge diameter multiple: 1.5
- sort order weight: -1
- sort sequence weight: -1

And I clicked the "calculate" button under "process".

A few seconds later the PCB outline's cut paths showed up in dark blue color. 


![](img/electronics-production/generating-the-milling-files/generating-the-milling-files-17.png)


In order to see the multiple tool passes, I rotated the PCB's view by keeping the mouse’s right button pressed and moving the mouse over the tool paths image. 


Taking into account that the "stock thickness" was 1.7 mm, and the "cut depth" was set to 0.6 by default, 3 passes were generated.



![](img/electronics-production/generating-the-milling-files/generating-the-milling-files-18.png)


Finally, I clicked the "save" button under "process", and I used the navigation window to save the milling job as "hello.ISP.44.traces.rml".




## **Milling the hello.ISP.44 PCB**

After having generated the two RML files needed to machine the hello.ISP.44 PCB, it was time to get close to one of the SRM-20 milling machines to start with the PCB machining operation.


### **Initializing the machine**

Nobody was using the machine when I started this part of the assignment, so the first thing I did was to turn on the computer connected to it.

While the computer was still booting, I turned on the Roland SRM-20 CNC milling machine by pressing the power button located at the top of the fairing.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-01.png)


As soon as I pressed the power button, the LED of the power button started to flash, and the initialization process started: the spindle head moved up (Z axis) and left (X axis), and the bed moved to the front (Y axis) of the machine. 

Once the initialization was completed, the LED of the power button stayed lit.



### **Setting up the machine**

With the monoFab SRM-20 initialized and the computer turned on, I launched the "VPanel for SRM-20" software application by clicking on the corresponding icon.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-02.png)


Once the "VPanel for SRM-20" program opened, the very first thing I did was to check the machine’s configuration by clicking the "Setup" button at the lower right of the program window.

Then, a "Setup" dialog window with 2 tabs ("Modeling Machine" and "Correction") popped up. 

In the "Modeling Machine" tab, I verified that "RML-1/NC Code" was selected under "Command Set", and that "Millimeters" was selected under "Unit".

Although it was not critical, I also checked that the preferred option "Move cutting tool to desired location" was selected under "Direction of the Y axis moving keypad".


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-03.png)


Next, I selected the "Correction" tab and checked that the correction values for the three axes available in that dialog window were set to "100%".


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-04.png)


Finally, I closed the "Setup" dialog window by clicking the “OK” button at the lower right corner.



### **Attaching the stock material**

After checking that the machine's parameters were correctly set, I started the preparation for attaching the stock material to the machine’s table.

I opened the machine’s lid and un-tightened the 4 screws that keep the table coupled to the Y axis. Then, I gently slid the table forward so it could be uncoupled from the screws and separated from the machine.

!!! Note 
    Attaching the stock material without removing the table from the machine was also possible, but removing it from the machine seemed much more convenient.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-05.png)


On the other hand, I though that it would be a good idea to warm up the spindle during the 2-3 minutes that I was going to spend attaching the stock material to the table. 

For that, I closed the machine’s lid and turned on the spindle by clicking the "ON" button under "Spindle" in the VPanel, setting the slide control under "Spindle Speed" to the lowest value.

!!! Info
    Warming up the spindle was not mandatory at all, but as far as I understood while reading the SRM-20’s user manual, it is a recommended practice for extending the life of the spindle.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-06.png)


While the spindle was warming up, I thoroughly cleaned the surface of the MDF sacrificial board that was taped down on top of the machine’s metallic bed. 

Luckily, there were no adhesive or double tape residues over the surface, so I just had to remove the dust particles using a soft brush.

Right after cleaning the surface, I also checked that the MDF sacrificial board was firmly attached to the bed, and that its surface was flat. Everything seemed to be ok! 


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-07.png)


The next step was attaching the FR1 PCB to the sacrificial board.

To do so, I cut a couple of pieces of double sided tape, and I applied them carefully all the way across the bottom side of the PCB, so as not to leave folds or overlaps.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-08.png)


Then, I removed the paper liner from the double sided tape, and I sticked the PCB on the MDF sacrificial board, aligning the PCB’s front side parallel to the bed's front side.

Once the PCB was correctly aligned, I used a piece of paper to press the PCB against the MDF, so it would be properly sticked without leaving fingerprint marks on the copper surface. 

!!! Important
    Fingerprints on the copper surface make it much harder to solder the SMD components to the board, so cleaning any fingerprint or spot on the copper surface before milling is highly recommended.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-09.png)


With the PCB correctly attached to the MDF, it was time to put the table back in the machine.

As the spindle was still warming up, I set the "spindle speed" parameter back to 100%, and I stopped the spindle head by clicking the "OFF" button under "Spindle" in the VPanel.

Once the spindle head stopped, I opened the machine’s lid and relocated the bed in place.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-10.png)



### **Inserting the milling bit**

With the table in position and the screws correctly tighten, I used VPanel's [X] and [Y] feed buttons to move the spindle head over the PCB. 

Then, I moved the spindle head up using the [Z] feed button in order to have enough space as to insert the milling bit, and to access to the mounting screw.

Later I grabbed the 1/64" milling bit and inserted approximately 1/3 of the shank into the spindle's collet, after checking that the tip was in good condition.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-11.png)



### **Setting the origin point**

After attaching the milling bit to the collet, I needed to set the origin point. 

In order to do so, I brought the tip of the milling bit near the front-left corner of the PCB, using VPanel’s [X] and [Y] feed buttons. 

Then, I used the [Z] feed button to slowly move the spindle head down, until the tip of the milling bit was approx. 5 mm above the left-front corner of the PCB.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-12.png)


Once the milling bit was at the right distance over the front-left corner of the PCB, I pressed the "X/Y" button under "Set Origin Point" in the VPanel, so the X and Y coordinate values were set to zero. 


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-13.png)


The origin point for X and Y axes was set, but I still had to set it for Z axis. 

Thus, I used an hex key to loosen the collet's set screw and, while holding the milling bit from the shank with two fingers, I allowed the bit to gently land on the PCB's surface.

Once the tip of the milling bit was in contact with the PCB, I tightened the collet screw.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-14.png)


Then, I clicked the "Z" button under "Set Origin Point"”" in the VPanel, so the Z coordinate in the left side of the program window was also set to zero. 


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-15.png)


At that point, the stock material was attached to the machine's bed, the 1/64" milling bit was inserted in the collet, and the origin point was set. 

Everything was ready to send the corresponding milling job!



### **Milling the traces**

To send the milling job to the monofab SRM-20, the first thing I did was clicking the "Cut" button at the lower right of the VPanel.

As soon as I clicked the "Cut" button, the "Cut" dialog window popped up on the screen.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-16.png)


Then, I clicked on the "Add" button at the lower-left side of the "Cut" dialog window, and a new navigation window opened.

Using that navigation window, I searched for the "hello.ISP.44.traces.rml" file that I had saved in the milling files generation stage, and I opened it.

Opening the file, it took me back to the "Cut" dialog window, where the selected file was listed in the "Output File List". 


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-17.png)


With the "hello.ISP.44.traces.rml" file listed in the "Output File List", I clicked the "Output" button at the lower-right hand of the "Cut" dialog window, and the machine started to run.

After a few minutes the milling job had finished, so I opened the machine’s lid and cleaned the PCB’s surface with a brush in order to check the outcome. 

Unfortunately, the layer of copper had not been completely removed in some areas of the PCB, which meant that (unlike it seemed before) the surface of the sacrificial board was not flat.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-18.png)



### **Flattening the sacrificial board**

In order to fix the "uneven waste-board issue" that I faced, I needed to prepare a milling job to flatten the surface of the sacacrifial board. 

The easiest way to do that was using Roland’s proprietary ClickMILL software, with one of the 1/8" 2FL flat-end milling bits that was available in Fab Lab Barcelona’s inventory.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-19a.png)


I started the waste board flattening process by removing the PCB from the sacrificial board.

Next, I replaced the 1/64" milling bit with the 1/8" milling bit, and I replicated the zeroing process that I had previously learned to set a new origin point in the scrap board.

Once the new origin point was set, I launched the ClickMILL application. 


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-19b.png)


In ClickMILL's program window, I selected the "Surface" tab, and I introduced the values for the "Lower left" and the "Upper right" corners of the sacrificial board. 

I also introduced the values for the (cutting) "Depth" and "Tool-up Height".


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-20.png)


Then, I clicked the "Cut" button in the lower-left side of the program window, and a new dialog window popped up, where I selected "Tool" and "Material".

Before continuing, I double-checked that the "Cutting parameters" set by default for the "Tool" and "Material" selected were the right one as to proceed with the flattening job. 

Finally, I clicked the "Cut" button in the lower left corner, and the milling job started.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-21.png)


After a long while, the surface of the sacrificial board was completely machined and allegedly flat, so I could resume the PCB milling process.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-22.png)



### **Milling the traces... again!**

The first thing I did in order to resume the milling job was to remove the old pieces of double sided tape that I had previously used to stick the PCB to the sacrificial board.

Then, I added new double-sided tape to the PCB, and I sticked it to the freshly milled sacrificial board taking into account the same details as before (alignment, no fingerprints).

Next, I removed the 1/8" milling bit that was still in the collet, and I replaced it with the 1/64" bit. 

Finally, I set a new origin point just next to my previous failed attempt.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-23.png)


Once the new origin point was set, I clicked the VPanel's "Cut" button.

The "Cut" dialog window popped up and the "hello.ISP.44.traces.rml" file was still in the "Output File List", so I just had to click the "Output" button to launch the PCB traces milling job again.

After a few minutes, the machining job ended. 

As soon as the spindle stopped spinning, I opened the machines lid, moved the table to the front, and cleaned the PCB’s surface using the brush. This time the outcome was much more satisfying!


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-24.png)


Considering the setbacks that I had with the uneven sacrificial board, I must admit I was quite happy having reached this point in the assignment.

Anyway, the PCB milling process was not finished yet. I still had to machine the PCB’s outline!



### **Milling the outline**

Changing the tool was the first thing to do for milling the PCB’s outline. 

I moved the spindle up, removed the 1/64" bit from the collet, and I inserted the 1/32" bit.

Then, I clicked the "X/Y" button under "Move > To Origin" in the VPanel, and the spindle head moved back to the origin point set for milling the PCB traces.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-25.png)


Next, I repeated Z axis zeroing process that I had used before.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-26.png)


After zeroing the Z axis, I clicked the VPanel's "Cut" button.

!!! Important
    Note that the "hello.ISP.44.traces.rml" file was still listed under "Output File List" in the "Cut" dialog window, so I had to click the "Delete" button to remove it from the list.

Next, I clicked on the "Add" button, searched for the "hello.ISP.44.interior.rml" file using the pop-up navigation window, and opened it.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-27.png)


Then, I clicked the "Output" button to run the machine again.

A few minutes later, the machining job had finished, so I opened the machines lid, moved the table to the front, and cleaned the PCB’s surface with a brush. 

As expected, the whole PCB was still attached to the scrap board, but it could be clearly seen that the PCB's outline had been completely cut. 


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-28.png)


 
### **Deburring the PCB**

After removing the hello.ISP.44 PCB from the sacrificial board with the help of a spatula, I cleaned the burr using a small piece of steel wool and cleaning alcohol.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-29.png)


Finally, my first PCB was done and it was not bad at all! However, I was totally sure that the outcome could be improved with a little more of practice. 

Since I had the whole PCB machining process under control, I repeated it a few times more, making some slight variations until I got a handful of hello.ISP.44 PCBs with a better result.


![](img/electronics-production/milling-the-hello.isp-pcb/milling-the-hello.isp-pcb-30.png)


!!! Info
    Note that I even etched a hello.ISP.44 PCB (see image above) using photosensitive positive PCBs, transfer paper, a UV-exposure unit, hydrogen peroxide, hydrochloric acid and a bunch of utensils,... but that is another (long) story! 



## **Assembling the Hello ISP board**

Once the hello.ISP.44 PCB was done, it was time for me to identify and collect the electronic components that I needed for assembling my hello.ISP.44 board. 



### **Collecting the parts**

With the help of the [hello.ISP.44 labeled board diagram](http://academy.cba.mit.edu/classes/embedded_programming/hello.ISP.44.png) and the [image of an assembled hello.ISP board](http://academy.cba.mit.edu/classes/embedded_programming/hello.ISP.44.components.png) that I had downloaded at the very beginning of this assignment, I made a list indicating the name of the parts and the quantity needed:


- 1x Mini-B USB connector
- 1x ATTiny 44 microcontroller
- 1x 6 pin header
- 1x 1uF capacitor
- 1x 10pF capacitor
- 2x 0 Ohm resistor
- 2x 100 Ohm resistor
- 1x 499 Ohm resistor
- 1x 1k Ohm resistor
- 1x 10k Ohm resistor
- 1x 20MHz crystal
- 2x 3.3V Zener diode

Besides, in order to find out more information about these parts, I searched for them in the [Fab Lab Inventory](https://docs.google.com/spreadsheets/d/1U-jcBWOJEjBT5A0N84IUubtcHKMEMtndQPLCkZCkVsU/pub?single=true&gid=0&output=html), where I obtained the [Digi-Key](https://en.wikipedia.org/wiki/Digi-Key) Part Number.

Knowing the corresponding Digi-Key Part Number for each of them, it allowed me to visit their product pages and learn more about them. It also helped me to see how they looked like.

- Mini-B USB connector → [Digi-key Part Number: H2961CT-ND](https://www.digikey.com/products/en?keywords=H2961CT-ND%09)
- ATTiny 44 microcontroller → [Digi-key Part Number: ATTINY44A-SSU-ND](https://www.digikey.com/products/en?keywords=ATTINY44A-SSU-ND%09)
- 6 pin ISP header → [Digi-key Part Number: 609-5161-1-ND](https://www.digikey.com/products/en?keywords=609-5161-1-ND%09)
- 1uF capacitor → [Digi-key Part Number: 445-1423-1-ND](https://www.digikey.com/products/en?keywords=445-1423-1-ND%09)
- 10pF capacitor → [Digi-key Part Number: 311-1150-1-ND](https://www.digikey.com/products/en?keywords=311-1150-1-ND%09)
- 0 Ohm resistor → [Digi-key Part Number: 311-0.0ERCT-ND](https://www.digikey.com/products/en?keywords=311-0.0ERCT-ND%09)
- 100 Ohm resistor → [Digi-key Part Number: 311-100FRCT-ND](https://www.digikey.com/products/en?keywords=311-100FRCT-ND%09)
- 499 Ohm resistor → [Digi-key Part Number: 311-499FRCT-ND](https://www.digikey.com/products/en?keywords=311-499FRCT-ND%09)
- 1k Ohm resistor → [Digi-key Part Number: 311-1.00KFRCT-ND](https://www.digikey.com/products/en?keywords=311-1.00KFRCT-ND%09)
- 10k Ohm resistor → [Digi-key Part Number: 311-10.0KFRCT-ND](https://www.digikey.com/products/en?keywords=311-10.0KFRCT-ND%09)
- 20MHz Crystal→ [Digi-key Part Number: 644-1039-1-ND](https://www.digikey.com/products/en?keywords=644-1039-1-ND%09)
- 3.3V Zener diode → [Digi-key Part Number: BZT52C3V3-FDICT-ND](https://www.digikey.com/products/en?keywords=BZT52C3V3-FDICT-ND%09)

After reviewing the product pages, I headed to the storage cabinets at Fab Lab Barcelona electronics' room, and I collected everything I needed.


![](img/electronics-production/assembling-the-hello-isp-board/assembling-the-hello-isp-board-01.png)


I spent a few minutes in front of the cabinets reviewing the labels and opening drawers, until I had collected the right amount of every component on the list.

As I was planning to assemble the board using the soldering tools that I had at home instead of the Fab Lab’s equipment, I put all the collected components in a small plastic box. 

!!! Tip
    Most of these components are very small, so keeping them inside a container would avoid loosing them.


![](img/electronics-production/assembling-the-hello-isp-board/assembling-the-hello-isp-board-02.png)


### **Collecting the tools**

When I arrived home with the hello.ISP.44 PCBs and the parts, I collected the necessary tools to solder the electronic components to the PCB:

- Soldering iron with a 0.5mm conical fine-point tip
- Soldering iron stand with sponge
- Distilled water
- 0.35 mm soldering wire
- Desoldering wick
- Tweezers
- Vacuum base PanaVise PCB holder
- Solder fume extractor
- Magnifying glass


![](img/electronics-production/assembling-the-hello-isp-board/assembling-the-hello-isp-board-03.png)


Luckily, I had experience soldering both [THT (Through-hole technology)](https://en.wikipedia.org/wiki/Through-hole_technology) and [SMD (surface-mount technology)](https://en.wikipedia.org/wiki/Surface-mount_technology) components, so I could start soldering straight away. 

I plugged in the soldering iron into the mains, and while the soldering iron was heating up, I took all the electronic components out from the blisters in which they came.

Then, I placed all of these parts near the PCB.


![](img/electronics-production/assembling-the-hello-isp-board/assembling-the-hello-isp-board-04.png)


### **Soldering the components**

After double checking that I had all the right parts for completing the hello.ISP.44 board, I held the PCB in the [PanaVise](http://panavise.com/index.html?pageID=1&id1=1&startat=1&--woSECTIONSdatarq=1&--SECTIONSword=ww), and I confirmed that the soldering iron was hot enough as to melt the ø0.35 mm soldering wire that I had chosen to use.

Then, I placed the fume extractor close to the PanaVise, turned it on, and started soldering.


![](img/electronics-production/assembling-the-hello-isp-board/assembling-the-hello-isp-board-05.png)


Following the [FabISP: Electronics Production tutorial](http://archive.fabacademy.org/archives/2017/doc/electronics_production_FabISP.html) available in the [Fab Academy tutorials page](http://archive.fabacademy.org/archives/2017/doc/index.html), I soldered the USB connector first. 


![](img/electronics-production/assembling-the-hello-isp-board/assembling-the-hello-isp-board-06.png)


The next component to be soldered was the ATTiny 44 micro controller. 

Although this component may seem much more hard to solder than the USB connector, it ended up being quite easy using the [“flood and wick” soldering technique](https://www.youtube.com/watch?v=8yyUlABj29o).


!!! Note
    When soldering the ATTiny 44 chip, it was was very important to keep in mind that it had to be positioned with a specific orientation. The little circle on the chip indicating the VCC pin should face the right direction.


![](img/electronics-production/assembling-the-hello-isp-board/assembling-the-hello-isp-board-07.png)


According to the tutorial, it came next the 20MHz Crystal. Then the diodes, the resistors, the capacitors, and the 6 pin header, in that order. 

The orientation of the crystal, resistors, capacitors, and the pin header did not matter because they do not have polarity. But the diodes' do have polarity so the orientation was important.

Knowing this, I preferred to skip the recommended order by the tutorial, and I soldered the two diodes first, ensuring that the tiny white line on the surface of the package indicating the cathode was facing the right direction.


![](img/electronics-production/assembling-the-hello-isp-board/assembling-the-hello-isp-board-08.png)


Then, I put some solder in one of the pads where the crystal, resistors, capacitors and header were going to be placed, and I soldered all these components carefully placing them on the right spot, but without paying close attention to their orientation, as I already knew that none of them would be problematic in terms of polarity.

I am not totally sure if this was there best way to do it, but it worked pretty well for me. After a few minutes of soldering, the hello.ISP.44 board was assembled, and it seemed to be OK!


![](img/electronics-production/assembling-the-hello-isp-board/assembling-the-hello-isp-board-09.png)


Anyway, before doing anything else, I used the magnifying glass to double check that there were not cold points, bridges, or any other common soldering problems with the assembled board. 

Everything still seemed to be OK, so I ventured myself to execute the last step of the FabISP: Electronics Production tutorial: the “Smoke Test”. 

I plugged the hello.ISP.44 board into my laptop using a mini-USB to USB-A cable, and there was no smoke, or any weird smell coming out from the board. Neither was any warning message from my laptop about the board drawing too much power.

Apparently, I could proceed to the next stage: programming the Hello ISP board!


![](img/electronics-production/assembling-the-hello-isp-board/assembling-the-hello-isp-board-10.png)




## **Programming the Hello ISP**

In order to program the Hello ISP  board, I followed the [FabISP: Programming tutorial](http://archive.fabacademy.org/archives/2017/doc/programming_FabISP.html#mac). 

According to that tutorial, the first thing I had to do was installing the corresponding software. 



### **Installing the software**

As I was using a macOS laptop for programming the Hello ISP board, I needed to use [CrossPack AVR](https://www.obdev.at/products/crosspack/index.html) in order to compile and upload the code to the ATtiny44A chip.

CrossPack AVR is a development environment for Atmel’s AVR micro controllers running on Apple’s Mac OS X, similar to AVR Studio on Windows. It consists of the GNU compiler suite, a C library for the AVR, the AVRDUDE uploader, and several other useful tools.


![](img/electronics-production/programming-the-hello-isp-board/programming-the-hello-isp-board-01.png)


From the CrossPack downloads page, I downloaded the [latest version available at that time](https://www.obdev.at/products/crosspack/download.html). 

Once the image file was completely downloaded, I opened it by double clicking the image file, and a new window popped up with a Readme file and the installer package.

I launched the installer file by double clicking on it, and I followed the installation instructions until the installation process was completed.


![](img/electronics-production/programming-the-hello-isp-board/programming-the-hello-isp-board-02.png)


In line with the [FabISP: Programming tutorial](http://archive.fabacademy.org/archives/2017/doc/programming_FabISP.html#mac), the next step was “getting Make via [XCode](https://itunes.apple.com/us/app/xcode/id497799835)”. 

I already had XCode installed, so I directly downloaded the [FabISP Firmware for MacOS 10.8.2](http://www.as220.org/fabacademy/downloads/fabISP_mac.0.8.2_firmware.zip) to my desktop, using the link available at Fab Academy’s Electronics Production page.

Once the download was completed, I unzipped the “fabISP_mac.0.8.2_firmware.zip” file, following the tutorial and using the Terminal program.

I launched the Terminal program, and I navigated to the desktop directory by typing:

    $ cd ~/Desktop/
    

![](img/electronics-production/programming-the-hello-isp-board/programming-the-hello-isp-board-03.png)


Then, I used the ```unzip``` command to decompress the “fabISP_mac.0.8.2_firmware.zip” file. 


    $ unzip fabISP_mac.0.8.2_firmware.zip

As soon as I hit Enter key, the console spit out a few code lines, indicating that the "fabISP_mac.0.8.2_firmware.zip" file had been uncompressed: 

    MacBook-Pro-de-Aitor:Desktop aitoraloa$ unzip fabISP_mac.0.8.2_firmware.zip
    Archive:  fabISP_mac.0.8.2_firmware.zip
       creating: fabISP_mac.0.8.2_firmware/
      inflating: fabISP_mac.0.8.2_firmware/.DS_Store  
       creating: __MACOSX/
       creating: __MACOSX/fabISP_mac.0.8.2_firmware/
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/._.DS_Store  
      inflating: fabISP_mac.0.8.2_firmware/main.c  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/._main.c  
      inflating: fabISP_mac.0.8.2_firmware/main.elf  
      inflating: fabISP_mac.0.8.2_firmware/main.hex  
      inflating: fabISP_mac.0.8.2_firmware/main.o  
      inflating: fabISP_mac.0.8.2_firmware/Makefile  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/._Makefile  
      inflating: fabISP_mac.0.8.2_firmware/usbconfig.h  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/._usbconfig.h  
       creating: fabISP_mac.0.8.2_firmware/usbdrv/
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/.DS_Store  
       creating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._.DS_Store  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/asmcommon.inc  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._asmcommon.inc  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/Changelog.txt  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._Changelog.txt  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/CommercialLicense.txt  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._CommercialLicense.txt  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/License.txt  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._License.txt  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/oddebug.c  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._oddebug.c  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/oddebug.h  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._oddebug.h  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/oddebug.o  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/Readme.txt  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._Readme.txt  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/USB-ID-FAQ.txt  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._USB-ID-FAQ.txt  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/USB-IDs-for-free.txt  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._USB-IDs-for-free.txt  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/usbconfig-prototype.h  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._usbconfig-prototype.h  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/usbdrv.c  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._usbdrv.c  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/usbdrv.h  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._usbdrv.h  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/usbdrv.o  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/usbdrvasm.asm  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._usbdrvasm.asm  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/usbdrvasm.o  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/usbdrvasm.S  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._usbdrvasm.S  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/usbdrvasm12.inc  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._usbdrvasm12.inc  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/usbdrvasm128.inc  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._usbdrvasm128.inc  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/usbdrvasm15.inc  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._usbdrvasm15.inc  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/usbdrvasm16.inc  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._usbdrvasm16.inc  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/usbdrvasm165.inc  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._usbdrvasm165.inc  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/usbdrvasm18-crc.inc  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._usbdrvasm18-crc.inc  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/usbdrvasm20.inc  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._usbdrvasm20.inc  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/USBID-License.txt  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._USBID-License.txt  
      inflating: fabISP_mac.0.8.2_firmware/usbdrv/usbportability.h  
      inflating: __MACOSX/fabISP_mac.0.8.2_firmware/usbdrv/._usbportability.h  
      

Next, I navigated to the "fabISP_mac.0.8.2_firmware" directory that I had just created by typing:

    $ cd fabISP_mac.0.8.2_firmware

Once in the "fabISP_mac.0.8.2_firmware" directory, I used the ```ls``` command in order to list the files inside that directory:

    $ ls

And the console prompted:

    Makefile        main.c        main.elf        main.hex        main.o        usbconfig.h        usbdrv

At that point, I had CrossPack and XCode installed, and the FabISP firmware files ready.

The next step was powering the Hello ISP board. 


### **Powering the Hello ISP**

!!! Important
    It would be impossible to program the Hello ISP 44 board without powering it first.

The right way to power the Hello ISP board was to connect it to the computer via USB cable, and to use a programmer plugged into the Hello ISP board's 6-pin programming header. 

That programmer could be a FabISP, or any other commercially available programmer, such the [Atmel AVRISP MkII](http://ww1.microchip.com/downloads/en/devicedoc/atmel-42093-avr-isp-mkii_userguide.pdf), the [Atmel-ICE](http://www.microchip.com/DevelopmentTools/ProductDetails.aspx?PartNO=atatmel-ice) or [Sparkfun's Pocket AVR Programmer](https://www.sparkfun.com/products/9825).


![](img/electronics-production/programming-the-hello-isp-board/programming-the-hello-isp-board-04.png)


I did not have a FabISP programmer yet, but I had an [Atmel AVRISP MkII](http://ww1.microchip.com/downloads/en/devicedoc/atmel-42093-avr-isp-mkii_userguide.pdf) at home, so my only option at that moment was using the AVRISP MkII programmer to power Hello ISP board.

Thus, I connected both the Hello ISP board and the AVRISP MkII programmer to my laptop using the corresponding USB cables. 

Then, I connected the AVRISP MkII programmer’s ISP female connector to the 6 pin male header of the Hello ISP board, being very careful to connect it right. 

!!! Important
    It was critical to connect the AVRISP MkII programmer and Hello ISP board correctly to avoid short-circuits.


![](img/electronics-production/programming-the-hello-isp-board/programming-the-hello-isp-board-05.png)


With the help of the [hello.ISP.44 labeled board diagram](http://academy.cba.mit.edu/classes/embedded_programming/hello.ISP.44.png), I figured out the pin layout for the 6 pin male header of the Hello ISP board. And visiting [Atmel’s AVRISP MkII Introduction page](http://www.atmel.com/webdoc/avrispmkii/avrispmkii.intro_connecting.html) at [Atmel’s documentation web site](http://www.atmel.com/webdoc/index.html), I also found out the AVR ISP connector pinout.

Once I had discovered the right orientation to hook up the AVRISP MkII programmer’s ISP connector into the Hello ISP board’s 6 pin header, I connected these two devices.

As soon as I connected them, the light indicator of the AVR ISP MkII changed from red to green, meaning that the Hello ISP board was correctly connected and powered.


![](img/electronics-production/programming-the-hello-isp-board/programming-the-hello-isp-board-06.png)


### **Editing the Makefile**

According to the [FabISP: Programming tutorial](http://archive.fabacademy.org/archives/2017/doc/programming_FabISP.html#mac), the next step after powering the hello ISP was to edit the "Makefile" file inside the “fabISP_mac.0.8.2_firmware” directory. 

Anyway, the "Makefile" file was set up by default to work with the AVRISP MkII, and as I was going to use a AVRISP MkII to program the Hello ISP board, I did not have to change it this time. 

At last, it was time to program the board! 


### **Compiling the firmware**

The first thing I did in order to program the Hello ISP board was to open a new Terminal window.

Then, I moved to the fabISP_mac.0.8.2_firmware directory:

    $ cd Desktop/fabISP_mac.0.8.2_firmware

And I compiled the firmware by typing:

    $ make clean 

According to the tutorial, the response that I received from the system was the right one:

    rm -f main.hex main.lst main.obj main.cof main.list main.map main.eep.hex 
    main.elf *.o usbdrv/*.o main.s usbdrv/oddebug.s usbdrv/usbdrv.s

Meaning that the firmware compilation had been done successfully.


### **Creating the hex file**

Immediately after the compilation, I used the ```make hex``` command in order to create the main.hex file (or output binary) that the AVR micro controller needed to run.

    $ make hex

And the response received from the system was:.

    avr-gcc -Wall -Os -DF_CPU=20000000         -Iusbdrv -I. -DDEBUG_LEVEL=0 
    -mmcu=attiny44 -c usbdrv/usbdrv.c -o usbdrv/usbdrv.o
    avr-gcc -Wall -Os -DF_CPU=20000000         -Iusbdrv -I. -DDEBUG_LEVEL=0 
    -mmcu=attiny44 -x assembler-with-cpp -c usbdrv/usbdrvasm.S -o usbdrv/usbdrvasm.o
    avr-gcc -Wall -Os -DF_CPU=20000000         -Iusbdrv -I. -DDEBUG_LEVEL=0 
    -mmcu=attiny44 -c usbdrv/oddebug.c -o usbdrv/oddebug.o
    avr-gcc -Wall -Os -DF_CPU=20000000         -Iusbdrv -I. -DDEBUG_LEVEL=0 
    -mmcu=attiny44 -c main.c -o main.o
    avr-gcc -Wall -Os -DF_CPU=20000000         -Iusbdrv -I. -DDEBUG_LEVEL=0 
    -mmcu=attiny44 -o main.elf usbdrv/usbdrv.o usbdrv/usbdrvasm.o usbdrv/oddebug.o 
    main.o
    rm -f main.hex main.eep.hex
    avr-objcopy -j .text -j .data -O ihex main.elf main.hex
    avr-size main.hex
       text           data            bss            dec            hex        filename
          0           2020              0           2020            7e4        main.hex 

Meaning that the hex file had been correctly created.

Once the firmware was compiled and the main.hex file created, it was time to set the fuses for the programmer to use the external clock (provided by the 20Mhz crystal). 


### **Setting the fuses**

In order to set the fuses in the micro controller, I typed:

    $ make fuse

And the response in the console was:

    avrdude -c usbtiny -p attiny44  -U hfuse:w:0xDF:m -U lfuse:w:0xFF:m
    
    avrdude: AVR device initialized and ready to accept instructions
    
    Reading | ################################################## | 100% 0.01s
    
    avrdude: Device signature = 0x1e9207
    avrdude: reading input file "0xDF"
    avrdude: writing hfuse (1 bytes):
    
    Writing | ################################################## | 100% 0.00s
    
    avrdude: 1 bytes of hfuse written
    avrdude: verifying hfuse memory against 0xDF:
    avrdude: load data hfuse data from input file 0xDF:
    avrdude: input file 0xDF contains 1 bytes
    avrdude: reading on-chip hfuse data:
    
    Reading | ################################################## | 100% 0.00s
    
    avrdude: verifying ...
    avrdude: 1 bytes of hfuse verified
    avrdude: reading input file "0xFF"
    avrdude: writing lfuse (1 bytes):
    
    Writing | ################################################## | 100% 0.01s
    
    avrdude: 1 bytes of lfuse written
    avrdude: verifying lfuse memory against 0xFF:
    avrdude: load data lfuse data from input file 0xFF:
    avrdude: input file 0xFF contains 1 bytes
    avrdude: reading on-chip lfuse data:
    
    Reading | ################################################## | 100% 0.00s
    
    avrdude: verifying ...
    avrdude: 1 bytes of lfuse verified
    
    avrdude: safemode: Fuses OK
    
    avrdude done.  Thank you.

Meaning that the fuses had been successfully set.


### **Flashing the Hello ISP**

Finally, I programmed the board. 

Flashing the board was as simple as typing:

    $ make program

The response that I received from the system was:

    avrdude -c usbtiny -p attiny44  -U flash:w:main.hex:i
    
    avrdude: AVR device initialized and ready to accept instructions
    
    Reading | ################################################## | 100% 0.01s
    
    avrdude: Device signature = 0x1e9207
    avrdude: NOTE: FLASH memory has been specified, an erase cycle will be performed
             To disable this feature, specify the -D option.
    avrdude: erasing chip
    avrdude: reading input file "main.hex"
    avrdude: writing flash (2020 bytes):
    
    Writing | ################################################## | 100% 5.68s
    
    avrdude: 2020 bytes of flash written
    avrdude: verifying flash memory against main.hex:
    avrdude: load data flash data from input file main.hex:
    avrdude: input file main.hex contains 2020 bytes
    avrdude: reading on-chip flash data:
    
    Reading | ################################################## | 100% 3.36s
    
    avrdude: verifying ...
    avrdude: 2020 bytes of flash verified
    
    avrdude: safemode: Fuses OK
    
    avrdude done.  Thank you.
    
    avrdude -c usbtiny -p attiny44  -U hfuse:w:0xDF:m -U lfuse:w:0xFF:m
    
    avrdude: AVR device initialized and ready to accept instructions
    
    Reading | ################################################## | 100% 0.01s
    
    avrdude: Device signature = 0x1e9207
    avrdude: reading input file "0xDF"
    avrdude: writing hfuse (1 bytes):
    
    Writing | ################################################## | 100% 0.00s
    
    avrdude: 1 bytes of hfuse written
    avrdude: verifying hfuse memory against 0xDF:
    avrdude: load data hfuse data from input file 0xDF:
    avrdude: input file 0xDF contains 1 bytes
    avrdude: reading on-chip hfuse data:
    
    Reading | ################################################## | 100% 0.00s
    
    avrdude: verifying ...
    avrdude: 1 bytes of hfuse verified
    avrdude: reading input file "0xFF"
    avrdude: writing lfuse (1 bytes):
    
    Writing | ################################################## | 100% 0.00s
    
    avrdude: 1 bytes of lfuse written
    avrdude: verifying lfuse memory against 0xFF:
    avrdude: load data lfuse data from input file 0xFF:
    avrdude: input file 0xFF contains 1 bytes
    avrdude: reading on-chip lfuse data:
    
    Reading | ################################################## | 100% 0.00s
    
    avrdude: verifying ...
    avrdude: 1 bytes of lfuse verified
    
    avrdude: safemode: Fuses OK
    
    avrdude done.  Thank you.

Which seemed to be ok, so my first hello ISO board was correctly programmed.

Anyway, I still needed to verify if the board would work.


### **Verifying my first FabISP**

The first thing I did to verify my FabISP board was to open my laptop’s [system profiler.](https://en.wikipedia.org/wiki/System_Information_(Mac))

Then, I used the left-hand side menu to navigate to "Hardware > USB > Hub".

In that dropdown menu, I could see one "FabISP" listed under the USB devices tree, meaning that the Hello ISP board had been correctly programmed.


![](img/electronics-production/programming-the-hello-isp-board/programming-the-hello-isp-board-07.png)



### **FabISP programmer**

Once the Hello ISP board was programmed, the only thing left to do was to remove the two Ohm resistors (SJ1 and SJ2) in order to set it as a FabISP programmer.

Thus, I plugged the soldering iron to the mains again, and while waiting for it to get hot, I placed the Hello ISP board back in the PanaVise holder. 

Then, I alternately heated each soldered side of the resistor, and removed the two resistors with the help of some tweezers. 

Finally, I used a piece of soldering wick to clean off the pads.


![](img/electronics-production/programming-the-hello-isp-board/programming-the-hello-isp-board-08.png)


I could then say that my FabISP programmer was finished, and allegedly working.

In any case, the best way to see if it really worked was using it to program other devices, so I decided that it would be nice to complete a second hello ISP board, using the FabISP programmer that I already owned. 

As I already knew the process, it would not be hard for me to repeat it, right!?  


![](img/electronics-production/programming-the-hello-isp-board/programming-the-hello-isp-board-09.png)



## **Making the IDC ISP cable**

Once I had the second Hello ISP board assembled, I found out that it would be necessary to make an ISP cable in order to connect the two Hello ISP boards via their 6 pin headers.

After reviewing some [former Fab Academy students’ documentation](http://fab.academany.org/2018/), I understood that I just needed a small piece of [ribbon cable](https://www.digikey.com/products/en?keywords=MC10M-50-ND%09) and two [6 pin (2x3) crimp-on IDC connectors](https://www.digikey.com/products/en?keywords=609-2841-ND%09) for making the suitable ISP cable to connect the two boards together. 


### **Collecting the parts**

Luckily, I saw a drawer with the "2x3 FEMALE CONNECTOR" label while I was checking the drawers looking for the hello.ISP.44 components. 

So, I went back to the Fab Lab Barcelona's electronics room, and took a few 6 pin IDC connectors (including the strain reliefs).


![](img/electronics-production/making-the-idc-isp-cable/making-the-idc-isp-cable-01.png)


At Fab Lab’s electronics room, there was also a big spool of ten way ribbon cable. 

I asked if there was also a six way ribbon cable available, but the answer was negative, so I had to use the 10 way ribbon spool in order to get the 6 way ribbon cable that I needed.


![](img/electronics-production/making-the-idc-isp-cable/making-the-idc-isp-cable-02.png)



### **10 way to 6 way ribbon cable**

The truth is that converting a ten way ribbon cable into a six way ribbon cable is not a big issue. It was as simple as removing four wires from the ten way ribbon cable. 

The first thing I did in order to do so, was to cut a piece of cable approximately 200 mm long from the ten way ribbon cable spool. 


![](img/electronics-production/making-the-idc-isp-cable/making-the-idc-isp-cable-03.png)


Then, I made a small cut between the yellow and the green wires using the cutting pliers.

!!! Note
    I could have cut between the blue and the purple wires too, but I preferred to keep the black wire at one side of the resulting six way flat cable in order to use it as ground reference later.


![](img/electronics-production/making-the-idc-isp-cable/making-the-idc-isp-cable-04.png)


Once the small cut was done, I separated the four wires from the other six by gently stretching both sets of wires in different directions.

The clear jacket joining the ten wires together was thinner between them, so pulling the wires apart happened to be very easy, since the separation itself was acting as a guide.


![](img/electronics-production/making-the-idc-isp-cable/making-the-idc-isp-cable-05.png)



### **Adding the IDC connectors**

Once I had the six wires ribbon cable, I double-checked that it had not defects.

Then, I put one of the six way ribbon cable's end through the IDC connector. 

When the ribbon cable's end was more or less aligned with the opposite side of the connector, I pressed the connector onto the ribbon cable.


![](img/electronics-production/making-the-idc-isp-cable/making-the-idc-isp-cable-06.png)


Normally, when making a regular IDC cable, we have to take into account the polarity. 

However, for this ISP cable that I was making, the polarity was not relevant. It was just important to keep in mind that the connection between the headers of the two Hello ISP boards had to be "straight-through". It meant: pin 1 to pin 1, pin 2 to pin 2, pin 3 to pin 3, etc.


![](img/electronics-production/making-the-idc-isp-cable/making-the-idc-isp-cable-07.png)


Punching the connector completely onto the ribbon cable by hand was impossible for me, and I did not have a proper IDC connector crimp tool at that moment, so I had to find another way to crimp it onto the ribbon cable. 

After watching [a couple of videos online about flat ribbon cable assemblies](https://www.youtube.com/watch?v=p1yZKT3Yock), I found out that the best way to crimp the 6 pin IDC connector was using a vise.


![](img/electronics-production/making-the-idc-isp-cable/making-the-idc-isp-cable-08.png)


Once I had the two IDC connectors crimped onto the ribbon cable, I attached the strain reliefs. 

Attaching the strain reliefs was not really necessary, as the ISP cable connection was not very susceptible of being accidentally pulled off, but I preferred to complete the IDC connector set.


![](img/electronics-production/making-the-idc-isp-cable/making-the-idc-isp-cable-09.png)



### **Checking the connections**

In order to check the cable connections, I grabbed a [digital multimeter](https://en.wikipedia.org/wiki/Multimeter).

I selected the multimeter's continuity test mode, and I tried to reach the contacts of the IDC connectors with the multimeter's probes. 

Sadly, reaching the contacts was not possible because the probes were not thin enough.

To solve this problem, I inserted two 0.35 mm solder pieces in the connectors' holes, and I checked the connections by touching the end of the solder pieces that jutted out.


![](img/electronics-production/making-the-idc-isp-cable/making-the-idc-isp-cable-10.png)


All the cable connections turned out to be fine, meaning that the IDC ISP cable was ready.

I could finally use my FabISP as a programmer! 


![](img/electronics-production/making-the-idc-isp-cable/making-the-idc-isp-cable-11.png)



## **Using the FabISP as programer**

In order to program my new Hello ISP board using the FabISP that I had previously programmed with the AVRISP MkII, I needed to replicate the whole programing process. 

Anyway, I had all the software dependencies installed, so the first thing I did in order to complete this last part of the assignment was powering the boards.


### **Powering the boards**

First, I connected both the FabISP programmer and the Hello ISP board to my laptop, using a pair of USB-A to mini-USB cables. 

Once the the FabISP programmer and the Hello ISP board were being powered by the computer, I connected them using the IDC ISP cable that I had just made.


![](img/electronics-production/using-the-fabisp-as-a-programer/using-the-fabisp-as-a-programer-01.png)


Before doing anything else, I double checked that the IDC ISP cable was correctly connecting the headers of the two boards. 

As mentioned before, the connection between headers had to be "straight-through", meaning that the pin 1 of the FabISP programmer had to be connected to the pin 1 of the Hello ISP board, the pin 2 of the FabISP programmer had to be connected to the pin 2 of the Hello ISP board, and so on.


![](img/electronics-production/using-the-fabisp-as-a-programer/using-the-fabisp-as-a-programer-02.png)


### **Editing the Makefile**

Retaking the "FabISP: Programming" tutorial, the next step was to edit the Makefile inside the "fabISP_mac.0.8.2_firmware" directory. 

As the Makefile was set up to work with the AVRISP MkII by default, but I was going to use a FabISP programer this time, I needed to edit it.

I opened the Makefile using TextEdit, and I searched for these 2 lines:

    #AVRDUDE = avrdude -c usbtiny -p $(DEVICE) # edit this line for your programmer
    AVRDUDE = avrdude -c avrisp2 -P usb -p $(DEVICE) # edit this line for your programmer


![](img/electronics-production/using-the-fabisp-as-a-programer/using-the-fabisp-as-a-programer-03.png)


Once I found them, I removed the hash symbol (#) in front of the line ```AVRDUDE = avrdude -c usbtiny -p $(DEVICE)``` , and I added a "hash" symbol (#) to the beginning of the line ```AVRDUDE = avrdude -c avrisp2 -P usb -p $(DEVICE)``` to comment it out.

    AVRDUDE = avrdude -c usbtiny -p $(DEVICE) # edit this line for your programmer
    #AVRDUDE = avrdude -c avrisp2 -P usb -p $(DEVICE) # edit this line for your programmer

Then, I saved the edited Makefile, overwriting the original one.


### **Programing a new Hello ISP**

Once the makefile was edited, I opened a new Terminal window and navigated to the fabISP_mac.0.8.2_firmware directory: 

    $ cd ~/Desktop/fabISP_mac.0.8.2_firmware

Next, I compiled the firmware, created the main.hex file, set the fuses, and flashed the new Hello ISP board, using the same commands that I had used to program the AVRISP MkII. 

Everything went fine, and the responses received were the right ones in every step.

    Last login: Mon Nov  6 16:31:20 on ttys000
    MacBook-Pro-de-Aitor:~ aitoraloa$ cd ~/Desktop/fabISP_mac.0.8.2_firmware
    MacBook-Pro-de-Aitor:fabISP_mac.0.8.2_firmware aitoraloa$ make clean
    rm -f main.hex main.lst main.obj main.cof main.list main.map main.eep.hex main.elf *.o usbdrv/*.o main.s usbdrv/oddebug.s usbdrv/usbdrv.s
    MacBook-Pro-de-Aitor:fabISP_mac.0.8.2_firmware aitoraloa$ make hex
    avr-gcc -Wall -Os -DF_CPU=20000000         -Iusbdrv -I. -DDEBUG_LEVEL=0 -mmcu=attiny44 -c usbdrv/usbdrv.c -o usbdrv/usbdrv.o
    avr-gcc -Wall -Os -DF_CPU=20000000         -Iusbdrv -I. -DDEBUG_LEVEL=0 -mmcu=attiny44 -x assembler-with-cpp -c usbdrv/usbdrvasm.S -o usbdrv/usbdrvasm.o
    avr-gcc -Wall -Os -DF_CPU=20000000         -Iusbdrv -I. -DDEBUG_LEVEL=0 -mmcu=attiny44 -c usbdrv/oddebug.c -o usbdrv/oddebug.o
    avr-gcc -Wall -Os -DF_CPU=20000000         -Iusbdrv -I. -DDEBUG_LEVEL=0 -mmcu=attiny44 -c main.c -o main.o
    main.c:88:13: warning: always_inline function might not be inlinable [-Wattributes]
     static void delay ( void )
                 ^
    avr-gcc -Wall -Os -DF_CPU=20000000         -Iusbdrv -I. -DDEBUG_LEVEL=0 -mmcu=attiny44 -o main.elf usbdrv/usbdrv.o usbdrv/usbdrvasm.o usbdrv/oddebug.o main.o
    rm -f main.hex main.eep.hex
    avr-objcopy -j .text -j .data -O ihex main.elf main.hex
    avr-size main.hex
    MacBook-Pro-de-Aitor:fabISP_mac.0.8.2_firmware aitoraloa$ make fuse
    avrdude -c usbtiny -p attiny44  -U hfuse:w:0xDF:m -U lfuse:w:0xFF:m
    
    avrdude: AVR device initialized and ready to accept instructions
    
    Reading | ################################################## | 100% 0.00s
    
    avrdude: Device signature = 0x1e9207
    avrdude: reading input file "0xDF"
    avrdude: writing hfuse (1 bytes):
    
    Writing | ################################################## | 100% 0.00s
    
    avrdude: 1 bytes of hfuse written
    avrdude: verifying hfuse memory against 0xDF:
    avrdude: load data hfuse data from input file 0xDF:
    avrdude: input file 0xDF contains 1 bytes
    avrdude: reading on-chip hfuse data:
    
    Reading | ################################################## | 100% 0.00s
    
    avrdude: verifying ...
    avrdude: 1 bytes of hfuse verified
    avrdude: reading input file "0xFF"
    avrdude: writing lfuse (1 bytes):
    
    Writing | ################################################## | 100% 0.00s
    
    avrdude: 1 bytes of lfuse written
    avrdude: verifying lfuse memory against 0xFF:
    avrdude: load data lfuse data from input file 0xFF:
    avrdude: input file 0xFF contains 1 bytes
    avrdude: reading on-chip lfuse data:
    
    Reading | ################################################## | 100% 0.00s
    
    avrdude: verifying ...
    avrdude: 1 bytes of lfuse verified
    
    avrdude: safemode: Fuses OK (H:FF, E:DF, L:FF)
    
    avrdude done.  Thank you.
    
    MacBook-Pro-de-Aitor:fabISP_mac.0.8.2_firmware aitoraloa$ make program
    avrdude -c usbtiny -p attiny44  -U flash:w:main.hex:i
    
    avrdude: AVR device initialized and ready to accept instructions
    
    Reading | ################################################## | 100% 0.00s
    
    avrdude: Device signature = 0x1e9207
    avrdude: NOTE: "flash" memory has been specified, an erase cycle will be performed
             To disable this feature, specify the -D option.
    avrdude: erasing chip
    avrdude: reading input file "main.hex"
    avrdude: writing flash (2002 bytes):
    
    Writing | ################################################## | 100% 2.03s
    
    avrdude: 2002 bytes of flash written
    avrdude: verifying flash memory against main.hex:
    avrdude: load data flash data from input file main.hex:
    avrdude: input file main.hex contains 2002 bytes
    avrdude: reading on-chip flash data:
    
    Reading | ################################################## | 100% 2.40s
    
    avrdude: verifying ...
    avrdude: 2002 bytes of flash verified
    
    avrdude: safemode: Fuses OK (H:FF, E:DF, L:FF)
    
    avrdude done.  Thank you.
    
    avrdude -c usbtiny -p attiny44  -U hfuse:w:0xDF:m -U lfuse:w:0xFF:m
    
    avrdude: AVR device initialized and ready to accept instructions
    
    Reading | ################################################## | 100% 0.00s
    
    avrdude: Device signature = 0x1e9207
    avrdude: reading input file "0xDF"
    avrdude: writing hfuse (1 bytes):
    
    Writing | ################################################## | 100% 0.00s
    
    avrdude: 1 bytes of hfuse written
    avrdude: verifying hfuse memory against 0xDF:
    avrdude: load data hfuse data from input file 0xDF:
    avrdude: input file 0xDF contains 1 bytes
    avrdude: reading on-chip hfuse data:
    
    Reading | ################################################## | 100% 0.00s
    
    avrdude: verifying ...
    avrdude: 1 bytes of hfuse verified
    avrdude: reading input file "0xFF"
    avrdude: writing lfuse (1 bytes):
    
    Writing | ################################################## | 100% 0.00s
    
    avrdude: 1 bytes of lfuse written
    avrdude: verifying lfuse memory against 0xFF:
    avrdude: load data lfuse data from input file 0xFF:
    avrdude: input file 0xFF contains 1 bytes
    avrdude: reading on-chip lfuse data:
    
    Reading | ################################################## | 100% 0.00s
    
    avrdude: verifying ...
    avrdude: 1 bytes of lfuse verified
    
    avrdude: safemode: Fuses OK (H:FF, E:DF, L:FF)
    
    avrdude done.  Thank you.
    
    MacBook-Pro-de-Aitor:fabISP_mac.0.8.2_firmware aitoraloa$ 


### **Verifying the new FabISP**

Once the flashing stage was completed, I verified that the new FabISP was working correctly.

With the 2 boards connected to my laptop, I opened the System Profiler again.

Under the USB devices tree, there were two FabISP devices listed, meaning that the newly flashed Hello ISP board had been correctly programmed.


![](img/electronics-production/using-the-fabisp-as-a-programer/using-the-fabisp-as-a-programer-04.png)




## **Completing the group project** 

I had already completed the whole PCB production process before, so this part of the assignment was mainly useful for checking that the mini CNC machines in the lab were still in good condition, and for helping my group members learn how the PCB production process was.

In any case, we started this group project by downloading the "trace-width" test files ([traces](http://academy.cba.mit.edu/classes/electronics_production/linetest.png) and [interior](http://academy.cba.mit.edu/classes/electronics_production/linetest.interior.png)) available at the [Electronics Production assignment page](http://academy.cba.mit.edu/classes/electronics_production/index.html).


![](img/electronics-production/completing-the-group-project/completing-the-group-project-01.png)


![](img/electronics-production/completing-the-group-project/completing-the-group-project-02.png)


Then, we used [Fab Modules](http://fabmodules.org/) to set the milling files, taking into account that we would mill the PCB traces using the 1/64 milling bit, and the PCB outline using the 1/32 milling bit.


![](img/electronics-production/completing-the-group-project/completing-the-group-project-03.png)


![](img/electronics-production/completing-the-group-project/completing-the-group-project-04.png)


Once the milling files were ready, and the circuit board stock properly attached to the mini CNC machine's bed, we launched the milling jobs using the VPanel software.

A few moments later the test file PCB was done, and the outcome was quite nice!


![](img/electronics-production/completing-the-group-project/completing-the-group-project-05.png)



## **What I have learned**

During this assignment, I basically learned what was a programmer and how to make one.

Besides, while making the FabISP, I learned about G-code, how to use Fab Modules, how to use a desktop milling machine, different techniques to solder SMD components into a PCB, and how to program a AVR micro controller.


## **Issues that I had**

It really was not an issue, but it was a pity that I could not use the Roland MDX-20 as I wanted

The main problem that I found during this assignment was that the SRM-20's sacrificial board was not flat, which forced me to learn how to use ClickMill in order to flatten it.

On the other hand, I found it difficult to crimp the IDC connectors and to test the connections while making the IDC ISP cable, but I was able to solve both issues by looking for solutions on the internet (and with a little bit of creativity).


## **Tools used** 

- **For preparing the RML files:**
    - MacOS High Sierra v10.13.3
    - Web-based Fab Modules


- **For milling the Hello.ISP board:**
    - Windows 7 Ultimate Service Pack 1
    - Roland SRM-20
    - Roland VPanel for SRM-20
    - Hex key
    - One-sided FR1 PCBs
    - 1/64” SE 2FL milling bit 
    - 1/32” SE 2FL milling bit
    - Double-sided tape
    - Spatula
    - Paper towels
    - Rubbing alcohol
    - Steel wool


- **For assembling the Hello.ISP board:**
    - Soldering iron with a 0.5mm conical fine-point tip
    - Soldering iron stand with sponge
    - Distilled water
    - 0.35 mm soldering wire
    - Desoldering wick
    - Tweezers
    - Vacuum base PanaVise PCB holder
    - Solder fume extractor
    - Magnifying glass


- **For programming the Hello.ISP board:**
    - MacBook Pro with macOS High Sierra v10.13.3
    - CrossPack-AVR v20131216
    - Xcode v9.2
    - Terminal v2.8.2
    - Atmel AVRISP MkII
    - USB-A to mini USB cable
    - USB-A to USB-B cable


- **For making the IDC ISP cable:**
    - 10 wires ribbon cable
    - 6 pin female crimp-on IDC connectors.
    - Scissors
    - Wire-cutter
    - PanaVise
    - Multimeter
    - 0.35 mm soldering wire


## **Files to download**
