# Networking and communications

Individual assignment: design and build a wired &/or wireless network connecting at least two processors

Group assignment: send a message between two projects


## **Which network board to make!?**

My plan was still to complete all of them but I started at the beginning and I made the hello.bus.45 boards, which meant making 1x hello.bus.45.bridge board, and 2x hello.bus.45.node boards.

I downloaded the PNG files, and the only difference between the bridge and node boards was that the bridge has 6 extra pins for connecting to the FTDI cable, while the nodes have only a 4 pin header to communicate. 

Note that the bridge gave power to other nodes.

![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525178217011_hello.bus.45.bridge.png)

![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525178226303_hello.bus.45.bridge.traces.png)
![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525178239957_hello.bus.45.bridge.interior.png)



![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525178424589_hello.bus.45.node.png)

![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525178432762_hello.bus.45.node.traces.png)
![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525178449971_hello.bus.45.node.interior.png)



## **Redrawing the hello.bus.45 boards**

repeated all the steps learned before when designing the previous boards

I created two new projects:

EAGLE → Projects > eagle > Networking and Communications > hello.bus.45.bridge

EAGLE → Projects > eagle > Networking and Communications > hello.bus.45.node

Designed the hello.bus.45.bridge schematic → components added from fab library


- 1x ATTINY45SI (Microcontroller)
- 1x FTDI-SMD-HEADER (FTDI header)
- 1x AVRISPSMD (ISP header)
- 1x PINHD-2x2-SMD (4 pin header)
- 1x LED1206FAB (Light Emitting Diode)
- 1x CAP-UAS1206FAB (Capacitor)
- 2x RES-US1206FAB (Resistor)


![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525180354422_Captura+de+pantalla+2018-05-01+a+las+15.12.11.png)

![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525196900872_Captura+de+pantalla+2018-05-01+a+las+19.47.37.png)


Laid out the hello.bus.45.bridge board


![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525196915763_Captura+de+pantalla+2018-05-01+a+las+15.57.25.png)

![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525197052516_Captura+de+pantalla+2018-05-01+a+las+19.50.35.png)


Checked the design rules for the hello.bus.45.bridge

Minimum clearance between objects in signal layers = 16mil

Everything ok!


![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525197426760_Captura+de+pantalla+2018-05-01+a+las+19.56.48.png)


Exported the hello.bus.45.bridge board

In the “Visible Layers” dialog Window, hide all layers except “Top” and “Dimension”.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525197178554_Captura+de+pantalla+2018-05-01+a+las+19.52.49.png)


File > Export > Image 

In the “Export Image” dialog window


- Monochrome
- Resolution = 500 dpi
- Area = Full



![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525200226554_hello.bus.45.bridge.traces.png)



Used Gimp for creating a white border around the image:

opened the exported PNG file
selected the PCB using the inner white line
“Image > “Crop to selection”
“Image > canvas size”

  Width = +20 
  Height = +20 
  Under “Offset”, click “Center” button

Click “Resize” button
Create a new layer with white background and place in under the traces layer
“Layer > Merge down”
“File > Export as….”


![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525203952367_hello.bus.45.bridge.traces_aftergimp.png)


And I also used Gimp to create the outline cut PNG file

selected the traces
“Edit > Fill with BG color” or “Edit > Fill with FG color”, depending which one is black
“Colors > Invert”
“File > Export as….”


![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525204464280_hello.bus.45.bridge.interior_aftergimp.png)



Then, I re-drawn the hello.bus.45.node schematic → components added from fab library


- 1x ATTINY45SI (Microcontroller)
- 1x AVRISPSMD (ISP header)
- 1x PINHD-2x2-SMD (4 pin header)
- 1x LED1206FAB (Light Emitting Diode)
- 1x CAP-UAS1206FAB (Capacitor)
- 2x RES-US1206FAB (Resistor)



![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525205315395_Captura+de+pantalla+2018-05-01+a+las+22.08.00.png)



![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525209612271_Captura+de+pantalla+2018-05-01+a+las+23.18.25.png)


Laid out the hello.bus.45.node board


![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525209676740_Captura+de+pantalla+2018-05-01+a+las+22.43.14.png)



![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525209683845_Captura+de+pantalla+2018-05-01+a+las+23.16.37.png)


Checkedthe design rules for the hello.bus.45. node board

Minimum clearance between objects in signal layers = 16mil

Everything ok!


![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525209802530_Captura+de+pantalla+2018-05-01+a+las+23.23.03.png)


Exported the hello.bus.45.node board

In the “Visible Layers” dialog Window, hide all layers except “Top” and “Dimension”.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525209896620_Captura+de+pantalla+2018-05-01+a+las+23.24.44.png)


File > Export > Image 

In the “Export Image” dialog window

Monochrome
Resolution = 500 dpi
Area = Full


![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525210158372_hello.bus.45.node.traces.png)


Used Gimp for creating a white border around the image:

Opened the PNG file
Selected the PCB using the inner white line
“Image > “Crop to selection”
“Image > canvas size”

  Width = +20 
  Height = +20 
  Under “Offset”, click “Center” button

Click “Resize” button
Create a new layer with white background and place in under the traces layer
“Layer > Merge down”
“File > Export as….”


![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525210550122_hello.bus.45.node.traces_aftergimp.png)


And I also used Gimp to create the outline cut *.png file

Selected the traces
“Edit > Fill with BG color” or “Edit > Fill with FG color”, depending which one is black
“Colors > Invert”
“File > Export as….”


![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525210583072_hello.bus.45.node.interior_aftergimp.png)




## **Making the hello.bus.45 boards**

### **Milling the boards**

Fab Modules
Roland SRM-20 Desktop Milling Machine
Rolans VPanel for SRM-20
one sided FR1 PCB
1/64” milling bit for the traces
1/32” milling bit for the outline


![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525244922836_IMG-3201.jpg)



### **Assembling the boards**

→ hello.bus.45.bridge


- 1x AVR ATTiny 45 Microcontroller IC 8-Bit 10MHz 4KB (2K x 16) FLASH 8-SOIC
- 1x 6 Positions Header Connector 0.100" (2.54mm) Surface Mount
- 1x 4 Positions Header Connector 0.100" (2.54mm) Surface Mount
- 1x 6 Positions Header Breakaway Connector 0.100" (2.54mm) Surface Mount, Right Angle
- 1x LED SMD 1206
- 1x 1µF ±10% 50V Ceramic Capacitor X7R 1206 SMD
- 1x 10k Ohm 1/4W 1% Resistor 1206 SMD
- 1x 1k Ohm 1/4W 1% Resistor 1206 SMD


- ATTiny 45 microcontroller → [Digi-key Part Number: ATTINY45V-10SU-ND](https://www.digikey.es/products/es?keywords=ATTINY45V-10SU-ND%09)
- 6 pin ISP header → [Digi-key Part Number: 609-5161-1-ND](https://www.digikey.com/products/en?keywords=609-5161-1-ND%09)
- 4 pin header → [Digi-key Part Number: 609-5160-1-ND](https://www.digikey.com/products/es?keywords=609-5160-1-ND)
- 6 pin FTDI header → [Digi-key Part Number: S1143E-36-ND](https://www.digikey.com/products/en?keywords=S1143E-36-ND)
- SMD LED → [Digi-key Part Number: 160-1167-1-ND](https://www.digikey.com/products/en?keywords=160-1167-1-ND)
- 1uF capacitor → [Digi-key Part Number: 445-1423-1-ND](https://www.digikey.com/products/en?keywords=445-1423-1-ND%09)
- 10k Ohm resistor → [Digi-key Part Number: 311-10.0KFRCT-ND](https://www.digikey.com/products/en?keywords=311-10.0KFRCT-ND%09)
- 1k Ohm resistor → [Digi-key Part Number: 311-1.00KFRCT-ND](https://www.digikey.com/products/en?keywords=311-1.00KFRCT-ND%09)


![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525246191569_IMG-3226.jpg)


→ hello.bus.45.node


- 1x AVR ATTiny 45 Microcontroller IC 8-Bit 10MHz 4KB (2K x 16) FLASH 8-SOIC
- 1x 6 Positions Header Connector 0.100" (2.54mm) Surface Mount
- 1x 4 Positions Header Connector 0.100" (2.54mm) Surface Mount
- 1x LED SMD 1206
- 1x 1µF ±10% 50V Ceramic Capacitor X7R 1206 SMD
- 1x 10k Ohm 1/4W 1% Resistor 1206 SMD
- 1x 1k Ohm 1/4W 1% Resistor 1206 SMD


- ATTiny 45 microcontroller → [Digi-key Part Number: ATTINY45V-10SU-ND](https://www.digikey.es/products/es?keywords=ATTINY45V-10SU-ND%09)
- 6 pin ISP header → [Digi-key Part Number: 609-5161-1-ND](https://www.digikey.com/products/en?keywords=609-5161-1-ND%09)
- 4 pin header → [Digi-key Part Number: 609-5160-1-ND](https://www.digikey.com/products/es?keywords=609-5160-1-ND)
- SMD LED → [Digi-key Part Number: 160-1167-1-ND](https://www.digikey.com/products/en?keywords=160-1167-1-ND)
- 1uF capacitor → [Digi-key Part Number: 445-1423-1-ND](https://www.digikey.com/products/en?keywords=445-1423-1-ND%09)
- 10k Ohm resistor → [Digi-key Part Number: 311-10.0KFRCT-ND](https://www.digikey.com/products/en?keywords=311-10.0KFRCT-ND%09)
- 1k Ohm resistor → [Digi-key Part Number: 311-1.00KFRCT-ND](https://www.digikey.com/products/en?keywords=311-1.00KFRCT-ND%09)


![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525246214618_IMG-3227.jpg)


Soldered the components to the boards


![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525246249419_IMG-3202.jpg)


## **Making the data cable**

10 wire ribbon cable to 4 wire ribbon cable


![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525246272355_IMG-3206.jpg)

![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525246284824_IMG-3208.jpg)

![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525246299479_IMG-3211.jpg)

![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525246309793_IMG-3215.jpg)

![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525246316956_IMG-3216.jpg)

![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525246326325_IMG-3218.jpg)

![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525246703579_IMG-3228.jpg)



## **Understanding the C code**

tbc


## **Programming the hello.bus.45 boards**

I created 3 directories:


- bridge
- node (1)
- node (2)

Copied the corresponding  C and MAKE files in each of the directories.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1527609738034_Captura+de+pantalla+2018-05-29+a+las+18.01.25+2.png)


**Programing the hello.bus.45.bridge board**

Connected the hello.bus.45.bridge to the computer using a FTDI cable

Connected the hello.ISP.44 to the computer using a miniUSB-A to USB-B cable

Connected the hello.bus.45.bridge and the hello.ISP.44 using the IDC cable


![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525246737190_IMG-3219.jpg)


Checked that everything is ok using the system profiler


![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1527593499598_Captura+de+pantalla+2018-05-29+a+las+12.33.24.png)


Opened terminal and navigated to the directory where the hello.bus.45.c and hello.bus.45.make files for the have been saved

Typed


    make -f hello.bus.45.make

Terminal response


    avr-gcc -mmcu=attiny45 -Wall -Os -DF_CPU=8000000 -I./ -o hello.bus.45.out hello.bus.45.c
    avr-objcopy -O ihex hello.bus.45.out hello.bus.45.c.hex;\
    avr-size --mcu=attiny45 --format=avr hello.bus.45.out
    AVR Memory Usage
    ----------------
    Device: attiny45
    
    Program:     768 bytes (18.8% Full)
    (.text + .data + .bootloader)
    
    Data:          4 bytes (1.6% Full)
    (.data + .bss + .noinit)

And the  “hello.bus.45.c.hex” and  “hello.bus.45.out” files had been created

Then, typed:


    make -f hello.bus.45.make program-usbtiny

Terminal response


    avr-objcopy -O ihex hello.bus.45.out hello.bus.45.c.hex;\
    avr-size --mcu=attiny45 --format=avr hello.bus.45.out
    AVR Memory Usage
    ----------------
    Device: attiny45
    
    Program:     768 bytes (18.8% Full)
    (.text + .data + .bootloader)
    
    Data:          4 bytes (1.6% Full)
    (.data + .bss + .noinit)
    
    
    avrdude -p t45 -P usb -c usbtiny -U flash:w:hello.bus.45.c.hex
    
    avrdude: AVR device initialized and ready to accept instructions
    
    Reading | ################################################## | 100% 0.01s
    
    avrdude: Device signature = 0x1e9206
    avrdude: NOTE: "flash" memory has been specified, an erase cycle will be performed
             To disable this feature, specify the -D option.
    avrdude: erasing chip
    avrdude: reading input file "hello.bus.45.c.hex"
    avrdude: input file hello.bus.45.c.hex auto detected as Intel Hex
    avrdude: writing flash (768 bytes):
    
    Writing | ################################################## | 100% 1.22s
    
    avrdude: 768 bytes of flash written
    avrdude: verifying flash memory against hello.bus.45.c.hex:
    avrdude: load data flash data from input file hello.bus.45.c.hex:
    avrdude: input file hello.bus.45.c.hex auto detected as Intel Hex
    avrdude: input file hello.bus.45.c.hex contains 768 bytes
    avrdude: reading on-chip flash data:
    
    Reading | ################################################## | 100% 1.77s
    
    avrdude: verifying ...
    avrdude: 768 bytes of flash verified
    
    avrdude: safemode: Fuses OK (H:FF, E:DF, L:62)
    
    avrdude done.  Thank you.

And the hello.bus.45.bridge had been programed


**Programing the hello.bus.45.node(1) board**

Connected  the hello.bus.45.bridge to the computer using a FTDI cable
Connect the hello.bus.45.bridge to first hello.bus.45.node sing the data cable
Connect the hello.ISP.44 to the computer using a miniUSB-A to USB-B cable
Connect the hello.bus.45.node (1) and the hello.ISP.44 using the IDC cable
Check that everything is ok using the system profiler


![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525246754169_IMG-3220.jpg)


Went  back terminal
Navigated to the directory where the hello.bus.45.c and hello.bus.45.make files for the hello.bus.45.node (1) have been saved.

Opened the hello.bus.45.c file and edit line 41


    #define node_id '0'

to 


    #define node_id '1' 

And saved.

Then, I typed


    make -f hello.bus.45.make

Terminal response


    avr-gcc -mmcu=attiny45 -Wall -Os -DF_CPU=8000000 -I./ -o hello.bus.45.out hello.bus.45.c
    avr-objcopy -O ihex hello.bus.45.out hello.bus.45.c.hex;\
    avr-size --mcu=attiny45 --format=avr hello.bus.45.out
    AVR Memory Usage
    ----------------
    Device: attiny45
    
    Program:     768 bytes (18.8% Full)
    (.text + .data + .bootloader)
    
    Data:          4 bytes (1.6% Full)
    (.data + .bss + .noinit)

And the  “hello.bus.45.c.hex” and  “hello.bus.45.out” for node 1 files have been created

Then, I typed:


    make -f hello.bus.45.make program-usbtiny

Terminal response


    avr-objcopy -O ihex hello.bus.45.out hello.bus.45.c.hex;\
    avr-size --mcu=attiny45 --format=avr hello.bus.45.out
    AVR Memory Usage
    ----------------
    Device: attiny45
    
    Program:     768 bytes (18.8% Full)
    (.text + .data + .bootloader)
    
    Data:          4 bytes (1.6% Full)
    (.data + .bss + .noinit)
    
    
    avrdude -p t45 -P usb -c usbtiny -U flash:w:hello.bus.45.c.hex
    
    avrdude: AVR device initialized and ready to accept instructions
    
    Reading | ################################################## | 100% 0.01s
    
    avrdude: Device signature = 0x1e9206
    avrdude: NOTE: "flash" memory has been specified, an erase cycle will be performed
             To disable this feature, specify the -D option.
    avrdude: erasing chip
    avrdude: reading input file "hello.bus.45.c.hex"
    avrdude: input file hello.bus.45.c.hex auto detected as Intel Hex
    avrdude: writing flash (768 bytes):
    
    Writing | ################################################## | 100% 1.22s
    
    avrdude: 768 bytes of flash written
    avrdude: verifying flash memory against hello.bus.45.c.hex:
    avrdude: load data flash data from input file hello.bus.45.c.hex:
    avrdude: input file hello.bus.45.c.hex auto detected as Intel Hex
    avrdude: input file hello.bus.45.c.hex contains 768 bytes
    avrdude: reading on-chip flash data:
    
    Reading | ################################################## | 100% 1.76s
    
    avrdude: verifying ...
    avrdude: 768 bytes of flash verified
    
    avrdude: safemode: Fuses OK (H:FF, E:DF, L:62)
    
    avrdude done.  Thank you.

And the hello.bus.45.node (1) had been programed


**Programing the hello.bus.45.node (2) board**

Connect the hello.bus.45.bridge to the computer using a FTDI cable
Connect the hello.bus.45.bridge to first hello.bus.45.node sing the data cable
Connect the hello.ISP.44 to the computer using a miniUSB-A to USB-B cable
Connect the hello.bus.45.node (2) board and the hello.ISP.44 using the IDC cable
Check that everything is ok using the system profiler


![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525246773625_IMG-3221.jpg)


Went back terminal
Navigated to the directory where the hello.bus.45.c and hello.bus.45.make files for the hello.bus.45.node (2) have been saved.

Opened the hello.bus.45.c file and edit line 41


    #define node_id '0'

to 


    #define node_id '2' 

And saved.

Then, I typed:


    make -f hello.bus.45.make

The terminal response was:


    avr-gcc -mmcu=attiny45 -Wall -Os -DF_CPU=8000000 -I./ -o hello.bus.45.out hello.bus.45.c
    avr-objcopy -O ihex hello.bus.45.out hello.bus.45.c.hex;\
    avr-size --mcu=attiny45 --format=avr hello.bus.45.out
    AVR Memory Usage
    ----------------
    Device: attiny45
    
    Program:     768 bytes (18.8% Full)
    (.text + .data + .bootloader)
    
    Data:          4 bytes (1.6% Full)
    (.data + .bss + .noinit)

And the  “hello.bus.45.c.hex” and  “hello.bus.45.out” for node 2 files had been created

Then, I typed:


    make -f hello.bus.45.make program-usbtiny

The terminal response was:


    avr-objcopy -O ihex hello.bus.45.out hello.bus.45.c.hex;\
    avr-size --mcu=attiny45 --format=avr hello.bus.45.out
    AVR Memory Usage
    ----------------
    Device: attiny45
    
    Program:     768 bytes (18.8% Full)
    (.text + .data + .bootloader)
    
    Data:          4 bytes (1.6% Full)
    (.data + .bss + .noinit)
    
    
    avrdude -p t45 -P usb -c usbtiny -U flash:w:hello.bus.45.c.hex
    
    avrdude: AVR device initialized and ready to accept instructions
    
    Reading | ################################################## | 100% 0.01s
    
    avrdude: Device signature = 0x1e9206
    avrdude: NOTE: "flash" memory has been specified, an erase cycle will be performed
             To disable this feature, specify the -D option.
    avrdude: erasing chip
    avrdude: reading input file "hello.bus.45.c.hex"
    avrdude: input file hello.bus.45.c.hex auto detected as Intel Hex
    avrdude: writing flash (768 bytes):
    
    Writing | ################################################## | 100% 1.23s
    
    avrdude: 768 bytes of flash written
    avrdude: verifying flash memory against hello.bus.45.c.hex:
    avrdude: load data flash data from input file hello.bus.45.c.hex:
    avrdude: input file hello.bus.45.c.hex auto detected as Intel Hex
    avrdude: input file hello.bus.45.c.hex contains 768 bytes
    avrdude: reading on-chip flash data:
    
    Reading | ################################################## | 100% 1.76s
    
    avrdude: verifying ...
    avrdude: 768 bytes of flash verified
    
    avrdude: safemode: Fuses OK (H:FF, E:DF, L:62)
    
    avrdude done.  Thank you.

And the hello.bus.45.node (2) board had been programed


## **Testing the hello.bus.45 boards**

Connected the three hello.bus.45 boards using the data cable


![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525246794696_IMG-3222.jpg)


Used terminal to navigate to the folder were the program term.py program was located

Checked the USB port were the board was connected.


    $ ls /dev/cu.*

Which prompted:


    /dev/tty.Bluetooth-Incoming-Port /dev/tty.usbserial-FTEZU7NB

Meaning that the hello.bus.45.bridge was connected to /dev/tty.usbserial-FTEZU7NB.


Then, I typed:


    python term.py  /dev/tty.usbserial-FTEZU7NB 9600

And as soon as I hit enter, a new dialog window called “term.py” opened:


![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525215753068_Captura+de+pantalla+2018-05-02+a+las+1.01.33.png)


Hit 0 → All boards blink but bridge blinks twice

Hit 1 → All boards blink but node 1 blinks twice

Hit 2 → All boards blink but node 2 blinks twice


![](https://d2mxuefqeaa7sj.cloudfront.net/s_F387BC91CA46DCCB21EE6E131FE37CB7F5883007346C27C7B2711379E7AEB796_1525215762493_Captura+de+pantalla+2018-05-02+a+las+1.02.17.png)




## **What I have learned**

[https://en.wikipedia.org/wiki/Bus_(computing)](https://en.wikipedia.org/wiki/Bus_(computing))

https://en.wikipedia.org/wiki/Serial_Peripheral_Interface_Bus

https://en.wikipedia.org/wiki/I%C2%B2C

https://learn.sparkfun.com/tutorials/serial-communication

https://learn.sparkfun.com/tutorials/serial-peripheral-interface-spi

https://learn.sparkfun.com/tutorials/i2c



## **Issues that I had**




## **Tools used**

- **For redrawing the boards:**
    - MacBook Pro with macOS High Sierra v10.13.3
    - EAGLE v8.0.2


- **For preparing the milling files:**
    - MacBook Pro with macOS High Sierra v10.13.3
    - GIMP v2.8.22
  
- **For making the boards:**
    - Milling 
        - Windows 7 Ultimate Service Pack 1
        - Roland VPanel for SRM-20
        - Roland SRM-20
        - Hex key
        - FR1 PCBs
        - 1/64” SE 2FL milling bit 
        - 1/32” SE 2FL milling bit
        - Double-sided tape
        - Spatula
        - Paper towels
        - Rubbing alcohol
        - Steel wool
    - Assembling
        - Soldering iron with a 0.5mm conical fine-point tip
        - Soldering iron stand with sponge
        - Distilled water
        - 0.35 mm soldering wire
        - Desoldering wick
        - Tweezers
        - Vacuum base PanaVise PCB holder
        - Solder fume extractor
        - Magnifying glass


- **For making the data cable:**
    - 10 wires ribbon cable
    - 4 pin female crimp-on IDC connectors.
    - Scissors
    - Wire-cutter
    - PanaVise
    - Multimeter
    - 0.35 mm soldering wire


- **For programming the  boards:**
    - MacBook Pro with macOS High Sierra v10.13.3
    - Terminal v2.8.2 (404)
    - FabISP programmer
    - USB-A to mini USB cable
    - FTDI Serial TTL-232 USB cable
    - 6 wire IDC ribbon cable
    - data cable


- **For testing the boards:**
    - MacBook Pro with macOS High Sierra v10.13.3
    - Python v2.7.10
    - term.py program
    - data cable
    - Multimeter
    - Osciloscope





## **Files to download**


