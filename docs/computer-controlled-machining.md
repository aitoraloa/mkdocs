# Week 07 Computer-Controlled Machining

Like the previous weeks, [Computer-controlled Machining week](http://academy.cba.mit.edu/classes/computer_machining/index.html) also included a group project. 

In this case, the group project was also oriented to test the machine that we were going to work with during this exercise. We needed to check the run out, alignment, speeds, feeds, and toolpath of the machine in order to verify that the machine was working well while learning how to use it.

On the other hand, the individual project involved making something big using the CNC machine. 



## **Planning to make something big**

It was quite hard for me to decide what to do as solo project for the individual assignment. There were so many things I wanted to fabricate with the CNC that I was totally blocked.

Luckily, the smart citizen team asked me to support them by fabricating a lectern that they needed for one of the [iScape project](https://www.iscapeproject.eu/) activities. I happily accepted the challenge!

**Project premises**

flat pack
lightweight
self-supporting
simple
tool-less
re-assemblable
movable

**References**

Searching for references, I picked:

Gypsy Modular
https://www.designboom.com/readers/gypsy-modular-clark-davis/
https://www.youtube.com/user/GypsyModular/videos

Sprout Modern Kids Furniture
https://sprout-kids.com/
https://www.youtube.com/channel/UCaNJ5CMlrugcn_Bc0ygrK7g

And after sketching a few ideas, I was totally sure that it could work.

Next, I needed to learn the tools.


## **Reviewing the available machines**

At the time of this writing there were two CNC machines available at Fab Lab Barcelona.

Both machine were suitable for making something big. 


**Precix 11100 series**


![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527785652663_IMG-3583.jpg)


work area: 3000 x 1500 x 300mm
feed: up to 30 cm/sec
accuracy: 0.02 mm
spindle power: 3hp
spindle speed: !?
control software: Mach3
materials: wood (plywood or solid wood), plastics and acrylics,…


**Shopbot**


![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527785669287_IMG-3585.jpg)


work area:  3660 x 1520 x 150 mm
feed: !?
accuracy: 0.01 mm
spindle power: 5.1hp HSD
spindle speed: 24000 rpm
control software: ShopBot Control Software
materials: wood (plywood or solid wood), plastics and acrylics, soft metals (aluminum, brass, copper),…

The Precix CNC machine seemed to have any kind of issues, so I decided to use the Shopbot.

Once I knew which machine I was going to use, I needed to learn how to use it.


## **Getting to know the tools**

Rhino → I already know how to use it

**Learning to use Rhino CAM**

watched [Mecasoft’s video tutorial](https://www.youtube.com/watch?v=MUoVU_MsEdg&t=47s)

practice with copy at Fab Lab computer


![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527852432463_Capture001.JPG)


**Learning to use the shopbot**

watched [Shopbot Training Videos](https://www.youtube.com/playlist?list=PL156AAB5A547C0D38)

readed [Shopbot Quick Start Guide](http://www.shopbottools.com/ShopBotDocs/files/QuickstartGuidePRSAlphaStandard.pdf)

Mikel from Fab Lab Barcelona staff taught me how to use it


![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527937557360_Untitled4.png)


**Selecting the cutting material**

Tried to recycle leftover material from other projects

Available leftovers: 


- 18mm plywood lacquered
- 15mm plywood

I finally decided to go for the 18mm lacquered plywood → more leftover material available


![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527786479738_IMG-3449.JPG)


**Selecting the cutting tool**

Checked the milling bits available at the fablab

Among the bits available, the right one seemed to be a 6mm single-flute upcut endmill 


- Cutting diameter: 6mm
- Shank Diameter: 6mm
- Overall length: 60mm
- Flute length: 22 mm

Ref. LMT-Belin 13060D ([LMT-Belin catalogue](http://www.perezcamps.com/ca/lmt-belin-tools))

With this type of miling bit the chips are ejected on the shank side of the tool.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527938126939_IMG-3658.jpg)



## **Finding the perfect slide friction joint**

Once that I had decided the joint method, selected the machines, material and milling bit, and roughly learned how to use RhinoCAM, Shopbot Control Center and the Shopbot CNC machine, it was time to work on achieving the best slide friction joint possible.

Besides, all the work that I was about to perform could be extrapolated to the lectern’s fabrication.


**Designing the test joints**

Used Rhino copy in my laptop

started  wit a slot width of 18,00 mm


![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527786641804_Captura+de+pantalla+2018-05-31+a+las+19.10.05.png)



**Preparing the milling files for the test joints**

Used the RhinoCAM 2015 copy available at Fab Lab Barcelona’s computer.

First, I used the copy of Rhino installed in the computer.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527938354885_Capture002.JPG)


Next, I launched  the RhinoCAM module by clicking on the “RhinoCAM 2015” button of the main menu bar, and selecting the “MILL” module in the dropdown menu. 

As soon as I selected “Mill” form the dropdown menu, the “Machining Browser” and the “Machining Objects” dialog windows appeared in the left of the screen program. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527852634936_Capture601.JPG)


At this point, I basically started following the instructions of the “[RhinoCAM 2015 MILL Quickstart](https://www.youtube.com/watch?v=LQdsXYhWWDk&t=2s)” video tutorial that I have watched before.

The first step was defining the Machine and the Post-Processor.

From the “Program” tab in the “Machining Browser” dialog window, I selected “Machine” in order to display the “Machine Tool Setup” dialog window. 

Under “Machine Type” in the “Machine Tool Setup” dialog window, I set the “Number of Axis” to “3 axis”, and I clicked “OK”.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527938461523_Capture602.JPG)


 
From the “Program” tab in the “Machining Browser” dialog window again, I selected “Post” to display the “Set Post-Processor Options” dialog window.

Under “Select Post Processor” I set “Current Post Processor” to “ShopBot_MTC”. And under “Posted File Naming Conventions”, I set “Posted File Extension” to SPB. 

Then, I clicked the “OK” button.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527938475696_Capture603.JPG)


Note that the post-processor and post file extension were determined by the machine that I was going to use. If I would have decided to use the Precix instead of the Shopbot, I would have had to use a different post-processor and file extension.

Once the Machine, the Post-Processor and the Post File Extension were correctly selected, it was time to define the machining set up.

The first step for defining the machining set up was **determining the raw stock material**.

From the “Program” tab in the “Machining Browser” dialog window, I selected “Box stock” from the “Stock” dropdown menu in order to display the “Box Stock” dialog window. 

Then, I clicked on the “Copy Model Bounding Box” button so the system analyzed the part and displayed the minimum dimensions of stock material that I needed.

And I clicked the “OK” button.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527938509306_Capture604.JPG)


The second step for defining the machining set up was **aligning the stock.**

From the “Program” tab of the “Machining Browser” dialog window, I selected “Align Stock” from the “Align” dropdown menu to open the “Align Part and/or Stock geometry” dialog window.

In the “Align Part and/or Stock geometry” dialog window, I selected “Top” under “Z alignment” and  “South-west” under “XY alignment”, and I clicked the “OK” button.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527938587368_Capture007.JPG)


The third step for defining the machining set up was **selecting the material.**

From the “Material” tab of the “Machining Browser” dialog window, I selected “Material” to open the “Select Stock Material” dialog window.

In the “Select Stock Material” dialog window, I selected “FeedSpeedDataMM.xml” under “Material File”, and “WOOD” under “Material”. 

Then, I clicked the “OK” button.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527938604882_Capture008.JPG)


The last step for defining the machining set up was **establishing the World Coordinate Origin**

Note that the World Coordinate Origin (WCS) is the origin point from where all the tool paths were going to be interpreted by the controller. 

From the “Program” tab of the “Machining Browser” dialog window, I selected “Align” to open the “Locate WCS with respect to Part or Stock” dialog window.

In the “Locate WCS with respect to Part or Stock” dialog window, I selected “Set to stock box” under “WCS Origin”, “Hightest Z” under “Zero face”, and “South West” under “Zero position”. 

Then, I clicked the “OK” button.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527938651129_Capture009.JPG)


Once the machining set up was completed, it was time to configure the machining tool.
 
Note that the Ø6mm 1Flute Upcut Flat-end milling bit that I was going to use, was already included in the “Tools Library”. In case I wanted to use a tool not included in the “Tools Library”, I would have had to include it.

From the “Tools” tab in the “Machining Objects” window, I clicked on the “Tools Machining Objects” button to display the “Create/Select Tool” window dialog.  

In the “Create/Select Tool” window dialog, I clicked the “Flat Mill” icon at the top menu. And from the tools available at “Tools in Session”, I selected “Flat 6mm”.

Then, I clicked the “OK” button.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527938690458_Capture010.JPG)


Once the machining set up was completed and the tool selected, it was time to create the different CNC milling strategies. 
 
I needed to create 3 strategies: 1 pocketing and 2 profiling

Selected “Machining Browser > Program tab > 2 axis > **Pocketing”**

  - This opened “2 1/2 Axis Pocketing” dialog window
  
    -  In the “Control Geometry” tab
      - Under “Drive Regions”
        - Clicked “Select Drive/Containment Regions” button
        - Selected the edge of the slide friction joint’s step

→ Captura Machining features/regions tab


    
    -  In the “Tool” tab
      - Under “Tools”
        - Selected “FlatMill 6”


~~**→ Captura “Tool” tab**~~



    - In the “Feeds and Speeds” tab
      - Clicked “load from tool” button
      - But also edited some speed parameters manually
      - Under “Spindle Parameters”
        - Speed = 15000
        - Direction = CW
      - Under “Feed rates”
        - Plunge = 2000
        - Aproach = 2000
        - Engage = 2000
        - Cut = 1500
        - Retract = 2000
        - Departure = 2000
        - Transfer = (Set) / 4000
      - Under “Feed Rate Reduction Factors”
        - Plunge between levels = 100%
        - First XY pass = 100%

~~**→ Captura “Feeds and Speeds” tab**~~



    - In the “Clearance Plane” tab
      - Under “Clearance Plane Definition”
        - Automatic
      - Under “Cut Transfer Method”
        - Clearance Plane


    ~~**→ Captura “Clearance Plane”tab**~~
    
    

    - In the “Cut Parameters” tab
      - Under “Global parameter”
        - Tolerance = 0.1
        - Stock = 0.635
        - Compensation= AUTO/NONE
      - Under “Cut Pattern”
        - Selected “Offset”
      - Under “Cut Direction”
        - Selected Mixed
      - Under “Start Point”
        - Selected Inside
      - Under “Stepover Distance”
        - % Tool. Dia. = 25


    ~~**→ Captura “Cut Parameters” tab**~~

          
      
    - In the “Cut Levels” tab
      - Under “Location of Cut Geometry”
        - Selected “At Top” 
      - Under “Cut Depth Control”
        - Total Cut Depth = 3
        - Rough Depth = 1
        - Finish Depth = 2
        - CutDepth/Cut = 1
        - Finish Depth/Cut = 1
      - Under “Cut Levels Ordering”
        - Selected “Depth First”


    ~~**→ Captura “Cut Levels” tab**~~


  
    - In the “Entry/Exit” tab
      - Under “Approach Motion”
        - Length = 0.6
      - Under “Engage Motion”
        - Selected “Path”
          - Angle (A) = 10
          - Height (H) = 0.6
      - Under “Retract Motion”
        - Selected “Linear”
          - Lenght (L) = 1.5
          - Angle (A) = 45
      - Under “Departure Motion”
        - Vertical Dist = 0.6


~~**→ Captura “Entry/Exit” tab**~~



    - In the “Advanced Cut Parameters” tab
      - By default


~~**→ Captura “Advanced Cut Parameters” tab**~~



    - In the “Sorting” tab
      - Selected “No Sort”


~~**→ Captura “Sorting” tab**~~


Clicked the “Generate” button at the bottom of the  “2 1/2 Axis Pocketing” dialog window, and the “2 1/2 Axis Pocketing” strategy was generated.

It could be seen under “Setup 1” in the “Machining Browser” dialog window.


~~**→ Captura de pantalla señalando la “2 1/2 Axis Pocketing” strategy**~~
   
   
Once the pocketing strategy was created, I moved on to the first Profiling strategy

Selected Machining Browser > Program tab > 2 axis > **Profiling**

  - This opened a “2 1/2 Axis Profiling” dialog window
  
    -  In the “Control Geometry” tab
      - Under “Drive Regions”
        - Clicked “Select Drive/Containment Regions” button
        - Selected the edge of the inner holes


~~**→ Captura Machining features/regions tab**~~


    
    -  In the “Tool” tab
      - Under “Tools”
        - Selected “FlatMill 6”


~~**→ Captura “Tool” tab**~~



    - In the “Feeds and Speeds” tab
      - Clicked “load from tool” button
      - But also edited some speed parameters manually
      - Under “Spindle Parameters”
        - Speed = 15000
        - Direction = CW
      - Under “Feed rates”
        - Plunge = 2000
        - Aproach = 2000
        - Engage = 2000
        - Cut = 1500
        - Retract = 2000
        - Departure = 2000
        - Transfer = (Set) / 4000
      - Under “Feed Rate Reduction Factors”
        - Plunge between levels = 100%
        - First XY pass = 100%


~~**→ Captura “Feeds and Speeds” tab**~~



    - In the “Clearance Plane” tab
      - Under “Clearance Plane Definition”
        - Automatic
      - Under “Cut Transfer Method”
        - Clearance Plane


    ~~**→ Captura “Clearance Plane”tab**~~
    
    

    - In the “Cut Parameters” tab
      - Under “Global parameter”
        - Tolerance = 0.03
        - Stock = 0
        - Compensation= AUTO/NONE
      - Under “Cut Direction”
        - Selected “Mixed”
      - Under “Cut Start Side”
        - Selected “Inside”
      - Under “Stepover Control”
        - Total Cut Width = 0
        - Step/Cut = 0


    ~~**→ Captura “Cut Parameters” tab**~~

          
      
    - In the “Cut Levels” tab
      - Under “Location of Cut Geometry”
        - Selected “At Top” 
      - Under “Cut Depth Control”
        - Total Cut Depth = 18
        - Rough Depth = 15
        - Finish Depth = 3
        - CutDepth/Cut = 3
        - Finish Depth/Cut = 1
      - Under “Cut Levels Ordering”
        - Selected “Depth First”


    ~~**→ Captura “Cut Levels” tab**~~


  
    - In the “Entry/Exit” tab
      - Under “Entry Motions”
        - Selected “None”
      - Under “Exit Motion”
        - Selected “Nones”


~~**→ Captura “Entry/Exit” tab**~~



    - In the “Advanced Cut Parameters” tab
      - By default


~~**→ Captura “Advanced Cut Parameters” tab**~~



    - In the “Sorting” tab
      - Selected “No Sort”


~~**→ Captura “Sorting” tab**~~


Clicked the “Generate” button at the bottom of the  “2 1/2 Axis Profiling” dialog window, and the “2 1/2 Axis Profiling” strategy was generated.

It could also be seen under “Setup 1” in the “Machining Browser” dialog window.


~~**→ Captura de pantalla señalando la “2 1/2 Axis Profiling” strategy**~~
   
   
Once the first profiling strategy was created, I moved on to the second profiling strategy.


Selected Machining Browser > Program tab > 2 axis > **Profiling**

  - This opened a new “2 1/2 Axis Profiling” dialog window
  
    -  In the “Control Geometry” tab
      - Under “Drive Regions”
        - Clicked “Select Drive/Containment Regions” button
        - Selected the outer edge of the parts


~~**→ Captura Machining features/regions tab**~~


    
    -  In the “Tool” tab
      - Under “Tools”
        - Selected “FlatMill 6”


~~**→ Captura “Tool” tab**~~



    - In the “Feeds and Speeds” tab
      - Clicked “load from tool” button
      - But also edited some speed parameters manually
      - Under “Spindle Parameters”
        - Speed = 15000
        - Direction = CW
      - Under “Feed rates”
        - Plunge = 2000
        - Aproach = 2000
        - Engage = 2000
        - Cut = 1500
        - Retract = 2000
        - Departure = 2000
        - Transfer = (Set) / 4000
      - Under “Feed Rate Reduction Factors”
        - Plunge between levels = 100%
        - First XY pass = 100%


~~**→ Captura “Feeds and Speeds” tab**~~



    - In the “Clearance Plane” tab
      - Under “Clearance Plane Definition”
        - Automatic
      - Under “Cut Transfer Method”
        - Clearance Plane


    ~~**→ Captura “Clearance Plane”tab**~~
    
    

    - In the “Cut Parameters” tab
      - Under “Global parameter”
        - Tolerance = 0.03
        - Stock = 0
        - Compensation= AUTO/NONE
      - Under “Cut Direction”
        - Selected “Mixed”
      - Under “Cut Start Side”
        - Selected “Outside”
      - Under “Stepover Control”
        - Total Cut Width = 0
        - Step/Cut = 0


    ~~**→ Captura “Cut Parameters” tab**~~

          
      
    - In the “Cut Levels” tab
      - Under “Location of Cut Geometry”
        - Selected “At Top” 
      - Under “Cut Depth Control”
        - Total Cut Depth = 18
        - Rough Depth = 15
        - Finish Depth = 3
        - CutDepth/Cut = 3
        - Finish Depth/Cut = 1
      - Under “Cut Levels Ordering”
        - Selected “Depth First”


    ~~**→ Captura “Cut Levels” tab**~~


  
    - In the “Entry/Exit” tab
      - Under “Entry Motions”
        - Selected “None”
      - Under “Exit Motion”
        - Selected “Nones”


~~**→ Captura “Entry/Exit” tab**~~



    - In the “Advanced Cut Parameters” tab
      - By default


~~**→ Captura “Advanced Cut Parameters” tab**~~



    - In the “Sorting” tab
      - Selected “No Sort”


~~**→ Captura “Sorting” tab**~~


Clicked the “Generate” button at the bottom of the  “2 1/2 Axis Profiling” dialog window, and the second “2 1/2 Axis Profiling” strategy was generated.

It could also be seen under “Setup 1” in the “Machining Browser” dialog window.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527941083099_Capture701.JPG)


Switched to the Simulate tab and clicked “play”

~~****~~
![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527941141046_Capture402.JPG)


Once the tool paths were completed, I generated the G-code to be sent to the machine.

Right-clicked over “Setup 1” and selected “Post” from the dropdown menu that appeared.

Used the “post & Save as” window browser that opened to save the SBP files in the cloud.

~~**→ captura de pantalla del G-Code generado**~~


**Milling the test joints**


- Turn-on the shopbot


  - Insert the key in the control box and turn it to the right


![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527941614995_IMG-3111.jpg)

  
  - Turn the red switch in the control box`
  
![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527941615409_IMG-3112.jpg)

  
  - Press "reset" button in the remote control
  
![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527941613992_IMG-3113.jpg)



- Set the spindle speed
  - Increased the rpm until it matched he speed set in the G-code files prepared in RhinoCAM
  
![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527941614295_IMG-3114.jpg)

  
- Initialized de machine
  - Used the "C3" command
  
![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527950029115_Untitled101.png)

  
  - The machine found the origin point in X and Y using the end stops
  
![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527950038630_Untitled102.png)



- Prepared the stock material in the machine
  - First I needed to move the spindle out from the middle
    - Opened Shopbot Control Center’s keypad.
    
![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527955241603_Untitled103.png)



    - Set a target points in X (2500) and Y (1000)
    
![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527951636389_Untitled104.png)



    - Pressed “Go To” button → carriage and gentry moved to the other end of the machine
    
![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527951831267_Untitled105.png)



    - Closed the keypad
    
![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527951907108_Untitled106.png)



  - Placed the stock material on top of the bed and align it 
  
![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527941784512_IMG-3122.jpg)



  - Used a drill driver and screws to fix the stock material in place
    (recommended to have the holes predrilled) 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527941835809_IMG-3123.jpg)

![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527941805599_IMG-3124.jpg)



- Prepared the milling bit in the chunk
  - selected the right collet
  
![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527941859999_IMG-3125.jpg)



  - cleaned the collet nut and the chunk


![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527941886031_IMG-3126.jpg)
![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527941885151_IMG-3127.jpg)

![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527941885584_IMG-3128.jpg)
![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527941885075_IMG-3129.jpg)



  - assembled the chunk
  
![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527941976177_IMG-3130.jpg)
![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527941975861_IMG-3131.jpg)

![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527941984653_IMG-3132.jpg)
![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527941984337_IMG-3133.jpg)



  - thoroughly tighten the chunk’s  nut  
  
![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527945819164_IMG-3134.jpg)



- **Placing the chunk in the spindle**


  - Sent the spindle back to machine’s origin point using the C3 command


![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527949021897_Untitled11.png)



  - Opened the keypad again
  - Moved the spindle in Z until there was enough space to insert the chunk


![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527955412590_Untitled107.png)



  - Inserted the chunk in the spindle
  
![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527945857442_IMG-3199.jpg)


Note that with the chunk inserted in the spindle I had to press “Outputs > 3” button in Shopbot Control center’s “Position” window in order to fix the chunk to the spindle. And that I would have to use this very same button to unfix the chunk from the spindle later.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527954042067_Untitled108.png)



- **Zeroing the X- and Y-axes**

****
  - Move the spindle down and brought the tip of the milling bit closer to the material as to get a god X/Y reference, but leaving a security height.
  - Used the Keypad to find the material/job origin point
  
![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527947073765_IMG-3648.jpg)



  - Taken note of the X and Y distances (form machine origin point to job origin point)
  
![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527954459051_Untitled109.png)



  - Clicked the "Zero Axes" button of the Keypad → open “Select Axes to Zero” dialog window
  
![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527954836580_Untitled110.png)

  
    - Selected X and Y in the “Select Axes to Zero” dialog window


![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527954915947_Untitled111.png)



    - Clicked the “ZERO” button to validate → closed “Select Axes to Zero” dialog window
    
![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527956114974_Untitled112.png)



    - Closed the Keypad


![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527956130717_Untitled113.png)

    
- **Zeroing the Z-axis**


  - Place the grounding clip on the bit on the collet nut 
  - Set the plate down directly beneath the bit
  
![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527956191738_IMG-3649.jpg)



  - Launched the C2 command 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527956556163_Untitled114.png)



  - Confirmed that the Z zeroing accessories were correctly placed, and clicked ok.


![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527957032455_Untitled115.png)



    Note that the bit touched the plate twice and then the Z axis moved up to a safe height.


  - Removed the Z zeroing accessories


- **Turned on the spindle’s air refrigeration system**


![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527848177772_IMG-3644.jpg)



- **Run the cut**

  - Clicked File > [P]ART FILE LOAD
  
![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527957925714_Untitled116.png)



  - Selected the SPB file in the window browser
  - Clicked “Open”.
  
![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527958051193_Untitled117.png)



  - In the “Fill-in” Sheet for [FP] command” that opened, I just hit the “Enter” key
  
![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527958109552_Untitled118.png)



  - A new window opened asking me to start the spindle


![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527958794540_Untitled119.png)



  - Before clicking “Ok”, I pressed the “Start” button on the three button pendant 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527958933774_IMG-3113.jpg)


And the cut started to run.


- While milling
  - Weared safety googles
  - Kept an eye on the machine all the time → for pressing the Reset button is necessary
  - Cleaned the excess chips being careful with the vacuum
  
![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527959013785_IMG-3651.jpg)


Once the milling was done

  - Moved the spindle out from the middle using the keypad
  - Thoroughly cleaned the chips using the vacuum cleaner
  
![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527959013118_IMG-3652.jpg)

  
  - Removed the bridges using a chisel and a hammer 
  
![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527959013670_IMG-3655.jpg)

****

**Finishing the test joints**


- Used a manual router with a tool for rounding edges with a 10mm radious.


- Used a fine sanding sponge to sand the rounded edges


![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527848273527_85540342-FB16-4283-8C3D-404DE63B7A0E.jpg)


**Testing the joints**

test 1 → working but too tight. 


![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527848327949_DE780649-1D35-4268-8A17-A381A0C036BB.jpg)


Repeated the whole process to make a second test 

test 2 → working fine

I was asked to remove the melamine from the milled parts


![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527848350246_AD5D28DE-0629-4D17-98F7-B58DE48A4B5D.jpg)



- Removed melamine cover
- Sanded flat surfaces with a belt sander

After removing the melamine the test 2 was too loose, and the test 1 was working perfect

~~****~~
![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527848350414_IMG-3099.jpg)




## **Fabricating the lectern**

**Designing the lectern**


- Used Rhino copy in my laptop
- Double checked form and function
- Triple checked dimensions


![](https://d2mxuefqeaa7sj.cloudfront.net/s_20F396E5E347702A0A7F2B5C24257DD025482A803A8B8531617DD1F7B2D16384_1527959381700_Captura+de+pantalla+2018-06-02+a+las+19.09.15+2.png)

****
**Preparing the milling files**


- Used Rhino and RhinoCAM in Fab Lab Barcelona’s computer
- Repeated the whole process learned while preparing the test joins
- Used same settings as with the test joints
****

****M**illing the lectern**


- Repeated the process learned while preparing the test joints
- Used a bigger stock of the same cutting material 
****

******Finishing the lectern**


- Used a manual router with a bit for rounding edges with a 10mm radius.
- Removed the melamine 
- Sanded flat surfaces with a belt sander
- Used a fine sanding sponge to sand the rounded edges


****
  **Assembling the lectern**


- put all the parts together
- added the electronics

~~**→ captura de pantalla del lectern terminado**~~


## **What I have learned**

CNC milling basics

types of materials that can be milled 

types of tools for CNC milling

How to use RhinoCAM

How to use a CNC machine

using a manual router


## **Issues that I had**

 
too small bridges in the first CNC milling strategies

maybe I should have used a smaller bit for some inner toolpaths

removing the melamine was a pain in the ass
 


## **Tools used**


- For designing
  - MacBook Pro with macOS High Sierra v10.13.3
  - Rhinoceros for Mac OS X Version 5.3.2


- For preparing the milling files
  - Windows 7 Ultimate Service Pack 1
  - Rhino
  - RhinoCAM


- For milling
  - Windows 7 Ultimate Service Pack 1
  - Shopbot Control Software v!?!?
  - Shopbot CNC machine
  - 6mm 1FL UPCUT milling bit
  - Screws
  - Safety glasses
  - Gloves
  - Respirator mask
  - Earmuffs
  - Vacuum cleaner



- For finishing
  - Hand-held router
  - Roundover bit
  - Belt sander
  - Sanding sponge
  - Clamps
  - Safety glasses
  - Gloves
  - Respirator mask
  - Earmuffs


- For assembling
  - Rubber hammer




## **Files to download**


****
